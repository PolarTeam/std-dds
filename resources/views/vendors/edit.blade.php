@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{trans('modVendor.titleName')}}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/vendors') }}">{{trans('modVendor.titleName')}}</a></li>
		<li class="active">{{trans('modVendor.titleName')}}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" class="form-control" id="session_id" name="session_id">
                                        <label for="vendor_no">{{trans('modVendor.vendorNo')}}</label>
                                        <input type="text" class="form-control" id="vendor_no" name="vendor_no" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="vendor_name">{{trans('modVendor.vendorName')}}</label>
                                        <input type="text" class="form-control" id="vendor_name" name="vendor_name" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="isEnabled">{{ trans('modVendor.isEnabled') }}</label>
                                        <select class="form-control" id="is_enabled" name="is_enabled">
                                            <option value="Y">{{trans('common.yes')}}</option>
                                            <option value="N">{{trans('common.no')}}</option>
                                        </select>
                                    </div>
                                    {{-- <div class="form-group col-md-3">
                                        <label for="vendor_address">供應商地址</label>
                                        <input type="text" class="form-control" id="vendor_address" name="vendor_address">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="vendor_attn">供應商聯絡人</label>
                                        <input type="text" class="form-control" id="vendor_attn" name="vendor_attn">
                                    </div> --}}
                                </div>
                                <div class="row"> 
                                    @if(isset($id))
                                        <input type="hidden" name="created_at" class="form-control">
                                        <input type="hidden" name="updated_at"  class="form-control">
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif
    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors";
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors/create";
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors";
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors";

    $(function(){
        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors" ;
        formOpt.backurl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/vendors" ;
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            // $('#role').select2();
            
        };
        formOpt.addFunc = function() {

        }

        formOpt.addFunc = function() {

        }
        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {

        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;
            var requiredColumn = [
                {
                    "column_filed":"vendor_name",
                    "column_type":"string",
                },
                {
                    "column_filed":"vendor_no",
                    "column_type":"string",
                },
            ];
            var cansafe = beforesave(requiredColumn) ;
            if(!cansafe) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }

            var content = $("#vendor_no").val();
            var c = new RegExp();
            c = /^[A-Za-z0-9]+$/;
            console.log(c.test(content));
            if(!c.test(content)){
                document.getElementById("vendor_no").style.backgroundColor = 'FCE8E6';
                swal("Code cannot be entered with non-alphanumeric characters", "", "warning");
                return false;
            }

            return cansafe;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);

    })
</script> 
<script>
$(function(){
});
</script>
@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>無揀貨儲位下載</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Pricing example for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/pricing/">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">

    <style>
        #block {
            height: 200px;
            width: 400px;
            background-color: white;
            font-size: 30px;
        
            position: absolute;     /*絕對位置*/
            top: 50%;               /*從上面開始算，下推 50% (一半) 的位置*/
            left: 50%;              /*從左邊開始算，右推 50% (一半) 的位置*/
            margin-top: -100px;     /*高度的一半*/
            margin-left: -200px;    /*寬度的一半*/
        
        }
    </style>
</head>

<body>
    <div class="container">

        <div id="block">
            <a href="{{$downloadUrl}}" target="_blank">下載無揀貨儲位</a>
        </div>
        

        <footer class="pt-4 my-md-5 pt-md-5 border-top">
        </footer>
    </div>
</body>

</html>
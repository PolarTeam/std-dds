@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 44px">
          <div class="pull-left image">
            {{-- <img src="http://placehold.it/160x160/00a7d0/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image"> --}}
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            {{--  <a href="#" id="onlineStatus"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          @can('Dashboard')
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/board') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          @endcan
          @can('Map')
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/map') }}" target="_blank"><i class="fa fa-map"></i> <span>{{ trans('menu.map') }}</span></a></li>
          @endcan
          @can('BasicInfo')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.basicInfo') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('customerProfile')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/customerProfile') }}"><i class="fa fa-group"></i> <span>{{ trans('menu.customerProfile') }}</span></a></li>
              @endcan
              @can('goodsProfile')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/goodsProfile') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.modGoods') }}</span></a></li>
              @endcan
              @can('companyProfile')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/companyProfile') }}"><i class="fa fa-building-o"></i> <span>{{ trans('menu.groupSetting') }}</span></a></li>
              @endcan
              @can('bulletin')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/bulletin') }}"><i class="fa fa-newspaper-o"></i> <span>{{ trans('menu.bulletin') }}</span></a></li>
              @endcan
              @can('bscodeKind')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/bscodeKind') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.bscode') }}</span></a></li>
              @endcan
              @can('mailFormat')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/mailFormat') }}"><i class="fa fa-envelope"></i> <span>{{ trans('menu.mailFormat') }}</span></a></li>
              @endcan
              @can('carProfile')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/carProfile') }}"><i class="fa fa-truck"></i> <span>{{ trans('menu.carProfile') }}</span></a></li>
              @endcan
              @can('sysCountry')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/sysCountry') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.sysCountry') }}</span></a></li>
              @endcan
              @can('sysArea')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/sysArea') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.sysArea') }}</span></a></li>
              @endcan
              @can('modTransStatus')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/modTransStatus') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.modTransStatus') }}</span></a></li>        
              @endcan
            </ul>
          </li>
          @endcan
          @can('Order')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.orderBasic') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('OrderMgmt')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmt') }}"><i class="fa fa-list-alt"></i> <span>{{ trans('menu.orderImportTest') }}</span></a></li>
              @endcan
              @can('CloseOrder')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/OrderMgmtclose') }}"><i class="fa fa-list-alt"></i> <span>結案管理</span></a></li>
              @endcan
              @can('ConfirmOrderMgmt')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/ConfirmOrderMgmt') }}"><i class="fa fa-list-alt"></i> <span>回單作業</span></a></li>
              @endcan
              @can('import')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/import') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.orderImport') }}</span></a></li>              
              @endcan
              @can('repairImport')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/repairimport') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.orderrepairImport') }}</span></a></li>
              @endcan    
              @can('OrderMgmt')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/ordOverViewN') }}"><i class="fa fa-list-alt"></i> <span>{{ trans('menu.orderMgmt') }}</span></a></li>
              @endcan
              @can('OrderDetailMgmt')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/ordDetailOverView') }}"><i class="fa fa-list-alt"></i> <span>{{ trans('menu.ordDetailOverView') }}</span></a></li>
              @endcan
              @can('ordDetailOverCloseView')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/ordDetailOverCloseView') }}"><i class="fa fa-list-alt"></i> <span>訂單明細總覽(結案)</span></a></li>
              @endcan
              @can('snno')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/snno') }}"><i class="fa fa-list-alt"></i> <span>序號綁定</span></a></li>
              @endcan
              @can('boxtask')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/boxtask') }}"><i class="fa fa-list-alt"></i> <span>工作站裝箱作業</span></a></li>
              @endcan
            </ul>
          </li>     
          @endcan
          @can('queryTracking')
          <li><a href="{{ url('/') . '/queryTracking?c='.Crypt::encrypt(Auth::user()->c_key.';'.Auth::user()->c_key) }}" target="_blank"><i class="fa fa-map-marker"></i> <span>{{ trans('menu.tracking') }}</span></a></li>
          @endcan
          @can('Tracking')
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/tracking') }}"><i class="fa fa-map-marker"></i> <span>{{ trans('menu.tracking') }}(內)</span></a></li>
          @endcan
          @can('TransportationPlan')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.transportationPlan') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('DlvCar')
              @endcan
              @can('DlvCarnew')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/DlvCarnew') }}"><i class="fa fa-truck"></i> <span>{{ trans('menu.sendCarDetail') }}</span></a></li>
              @endcan
              @can('SendCar')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/SendCar') }}"><i class="fa fa-truck"></i> <span>{{ trans('menu.sendCar') }}</span></a></li> 
              @endcan
              @can('DlvPlanSearch')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/TranPlanMgmt/DlvPlanSearch') }}"><i class="fa fa-truck"></i> <span>{{ trans('menu.dlvPlanSearch') }}</span></a></li> 
              @endcan
              @can('Autosendcar')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/autosendcar') }}"><i class="fa fa-truck"></i> <span>自動派車建檔</span></a></li> 
              @endcan
            </ul>
          </li>  
          @endcan
          @can('Quote')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.quotMgmt') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/sysRefFeeCar') }}"><i class="fa fa-file"></i> <span>{{ trans('menu.sysRefFeeCar') }}</span></a></li>
            </ul>
          </li>
          @endcan

          {{-- @can('setting')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.sysSetting') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/apiMgmt') }}"><i class="fa fa-files-o"></i> <span>API Mgmt.</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('menu.fileMgmt') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language') }}"><i class="fa fa-flag-o"></i> <span>{{ trans('menu.langSetting') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/language/texts') }}"><i class="fa fa-language"></i> <span>{{ trans('menu.langFile') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}"><i class="fa fa-hdd-o"></i> <span>{{ trans('menu.backup') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/log') }}"><i class="fa fa-terminal"></i> <span>{{ trans('menu.logs') }}</span></a></li>              
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/setting') }}"><i class="fa fa-cog"></i> <span>{{ trans('menu.settings') }}</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/page') }}"><i class="fa fa-file-o"></i> <span>{{ trans('menu.pages') }}</span></a></li>
              <li><a href="{{ url('admin/menu-item') }}"><i class="fa fa-list"></i> <span>Menu</span></a></li>              
            </ul>
          </li>
          @endcan --}}
          <!-- Users, Roles Permissions -->
          @can('Permissions')
          <li class="treeview">
            <a href="#"><i class="fa fa-folder"></i> <span>{{ trans('menu.permissions') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              @can('AdminPermissions')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-user"></i> <span>{{ trans('menu.users') }}</span></a></li>
              @endcan
              @can('NormalPermissions')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/custuser') }}"><i class="fa fa-user"></i> <span>{{ trans('menu.custusers') }}</span></a></li>
              @endcan
              
              @can('AdminPermissions')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/role') }}"><i class="fa fa-group"></i> <span>{{ trans('menu.roles') }}</span></a></li>
              @endcan
              @can('AdminPermissions')
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/permission') }}"><i class="fa fa-key"></i> <span>{{ trans('menu.permission') }}</span></a></li> 
              @endcan             
            </ul>
          </li>
          @endcan
          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif

@section('sidebar_scripts')
<script>
  /* 進入頁面開啟目前頁面右邊選單所屬分類 */
  let locationHref = location.href.split('/')[4];
  let treeviewList = document.querySelectorAll('.treeview');
  let treeviewListLength = treeviewList.length;
  let activeIndex = null;
  for (let i = 0; i < treeviewListLength; i++) {
    // console.log(treeviewList[i].children[1].children);
    let ulList = treeviewList[i].children[1].children;
    let ulListLength = ulList.length;
    // console.log(ulList);
    for (let index = 0; index < ulListLength; index++) {
      let link = ulList[index].children[0].href.split('/')[3]
      if (locationHref.indexOf(link) !== -1) {
        activeIndex = i;
        ulList[index].children[0].style.color = '#fff';
      }
    }
  }
  // console.log(activeIndex);
  if (activeIndex !== null) {
    treeviewList[activeIndex].classList.add('active');
  }
  /* 進入頁面開啟目前頁面右邊選單所屬分類 END */
</script>
@endsection

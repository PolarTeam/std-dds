@extends('layout.layout')

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.42/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.3/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.css">

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.3/material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="{{ asset('vendor/ejs') }}/ejs.min.js"></script>
<div class="dashboard">
    <div class="border-box">
        <div class="content-box">
            <div class="content-header p-0">
                <input type="button" style="display:none" id="updategrid" >
                <h1 class="mb-3" style='display: inline-block;'>
                    {{ trans('dashboard.titleName') }}<small></small>
                </h1>
            </div>
            <div class="btn-box">
                <button type="button" onclick="borrowurl()" class="icon-btn"><i class="fa fa-file-o" aria-hidden="true"></i> {{trans('dashboard.CO')}}</button>
                <button type="button" onclick="mymaterialsurl()" class="icon-btn"><i class="fa fa-archive" aria-hidden="true"></i>{{ trans('dashboard.RT') }}</button>
                <button type="button" onclick="mymaterialsurl()" class="icon-btn"><i class="fa fa-refresh" aria-hidden="true"></i>{{ trans('dashboard.TF') }}</button>
                <button type="button" onclick="mymaterialsurl()" class="icon-btn"><i class="fa fa-calendar-o" aria-hidden="true"></i>{{ trans('dashboard.PP') }}</button>
                <button type="button" onclick="mymaterialsurl()" class="icon-btn"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>{{ trans('dashboard.SC') }}</button>
            </div>
            <ul class="list-unstyled">
                <li>
                    <button type="button" class="list-btn" onclick="myapplicationForm()">
                        <span class="news-dot" id="newsDot-1"></span>
                        <span class="lable">{{trans('dashboard.myapplicationForm')}}</span>
                        <span>
                            <span class="num">{{$viewData['myapplicationForm']}} {{trans('dashboard.unit1')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                <li>
                    <button type="button" class="list-btn" onclick="mymaterialsurl()">
                        <span class="news-dot" id="newsDot-2"></span>
                        <span class="lable">{{trans('dashboard.mymaterials')}}</span>
                        <span>
                            <span class="num">{{$viewData['mymaterials']}} {{trans('dashboard.unit2')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                <li>
                    <button type="button" class="list-btn" onclick="mymaterialsurl()">
                        <span class="news-dot" id="newsDot-3"></span>
                        <span class="lable">{{trans('dashboard.expiringmaterial')}}</span>
                        <span>
                            <span class="num">{{$viewData['expiringmaterial']}} {{trans('dashboard.unit2')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                <li>
                    <button type="button" class="list-btn" onclick="myapplicationForm()">
                        <span class="news-dot" id="newsDot-4"></span>
                        <span class="lable">{{trans('dashboard.successapplicationForm')}}</span>
                        <span>
                            <span class="num">{{$viewData['pendingdelivery']}} {{trans('dashboard.unit1')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                <li>
                    <button type="button" class="list-btn" onclick="inventoryprofile()">
                        <span class="news-dot" id="newsDot-5"></span>
                        <span class="lable">{{trans('dashboard.inventoryapplicationForm')}}</span>
                        <span>
                            <span class="num">{{$viewData['inventoryprofile']}} {{trans('dashboard.unit1')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
               
                @if ($viewData['iskeeper'] == "Y")
                <li>
                    <button type="button" class="list-btn" onclick="applicationFormbykeeper()">
                        <span class="news-dot" id="newsDot-6"></span>
                        <span class="lable">{{trans('dashboard.keeperrequisition')}}</span>
                        <span>
                            <span class="num">{{$viewData['keeperpendingapplication']}} {{trans('dashboard.unit1')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                @endif
                @if ($viewData['isapprover'] == "Y")
                <li>
                    <button type="button" class="list-btn" onclick="applicationFormbyapprover()">
                        <span class="news-dot" id="newsDot-7"></span>
                        <span class="lable">{{trans('dashboard.approverrequisition')}}</span>
                        <span>
                            <span class="num">{{$viewData['approverpendingapplication']}} {{trans('dashboard.unit1')}}</span>
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>

{{-- <div class="row">
    <div class="col-md-12">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('dashboard.onlineTruck') }}</h3>
<div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
</div>
</div>
<div class="box-body" id="transports">
</div>
</div>
</div>
</div> --}}
@can('gpscar')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title" style="display:inline;">司機在線狀況</h3> <button type="button" style="float: right;" id="updatedriver">即時更新 </button> <button type="button" style="float: right;" id="sendmsg">傳送訊息 </button>
            </div>
            <div id="jqxGrid" class="col-md-12">
                <table id="tab0" style="display:inline;width:80%;">
                    <tr id="tra" style='width:15%;'>
                    </tr>
                </table>
                <table id="tab1" style="display:inline;width:80%;">
                    <tr id="tr0" style='width:15%;'>
                    </tr>
                </table>
                <table id="tab2" style="display:inline;width:50%">
                    <tr id="trx" style='width:15%;'>
                    </tr>
                </table>
                <table id="tab3" style="display:inline;width:50%">
                    <tr id="try" style='width:15%;'>
                    </tr>
                </table>
                <table id="tab4" style="display:inline;width:50%">
                    <tr style='width:15%;'>
                    </tr>
                </table>
                <table id="tab5" style="display:inline;width:50%">
                    <tr style='width:15%;'>
                    </tr>
                </table>
                <table id="tab6" style="display:inline;width:50%">
                    <tr style='width:15%;'>
                    </tr>
                </table>
                <table id="tab7" style="display:inline;width:50%">
                    <tr style='width:15%;'>
                    </tr>
                </table>
                <table id="tab8" style="display:inline;width:50%">
                    <tr style='width:15%;'>
                    </tr>
                </table>
            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endcan
<div class="modal fade" role="dialog" id="dlvPlanModal">
    <div class="modal-dialog modal-lg" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">訊息通知</h4>
            </div>
            <div class="modal-body" id="dlvPlanBody" style="height: 200px;width:100%">
                <div class="row">
                    <div class="form-group col-md-12">
                        <select class="form-control select2" style="width: 100%; text-align:center" id='carno' name="state">

                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="goods_no2">內容</label>
                        <textarea class="form-control" id='msgcontent' rows="3" name="remark"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="confirmInsertBtn">確定發送</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.close') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script id="transports-template" type="x-ejs">
    <% transports.forEach(function(transport) { %>
<div class="mdl-card" style="background:rgb(250, <%= Math.round(transport.power * 2.5 ) %>, <%= Math.round(transport.power * 2.5 ) %>)">
  <div class="mdl-card__title">
    <h3 class="mdl-card__title-text"><%= transport.id %></h3>
  </div>
  <div class="mdl-card__actions">
  <img src="<%= transport.map %>">
  <ul>
    <!--<li><label>Location:</label><%= transport.lat %>,<%= transport.lng %></li>-->
    <li><label>{{ trans('dashboard.battery') }}:</label><%= transport.power %>%</li>
    <li><label>{{ trans('dashboard.speed') }}:</label><%= transport.speed %>km/h</li>
    <li><label>{{ trans('dashboard.updated') }}:</label><%= transport.time %></li>
  </ul>
  </div>
</div>
<% }) %>
</script>


<style>
    .content {
        overflow-y: auto;
    }
    .content-wrapper {
        background-color: #032640;
    }
    .content-header>h1 {
        color: #a6b1d3;
    }
    .dashboard {
        position: relative;
        width: 100%;
        height: 100%;
    }
    .dashboard::before {
        content: "";
        display: block;
        position: absolute;
        width: 100px;
        height: 90%;
        background: #003059;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) skewX(330deg);
        box-shadow:0 0 30px 30px #003059;
        z-index: 0;
    }
    .dashboard::after {
        content: "";
        display: block;
        position: absolute;
        width: 100px;
        height: 90%;
        background: #7ED4F1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) skewX(30deg);
        box-shadow:0 0 20px 30px #7ED4F1;
        opacity: .5;
        z-index: 0;
    }
    .border-box {
        position: relative;
        margin: 24px;
        padding: 48px 52px;
        border: 1px solid #047ed1;
        background: #66000000;
        background: -moz-linear-gradient(top, #66000000 0%, #00000000 100%);
        background: -webkit-linear-gradient(top, #66000000 0%,#00000000 100%);
        background: linear-gradient(to bottom, #66000000 0%,#00000000 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#66000000', endColorstr='#00000000',GradientType=0 );
        backdrop-filter: blur(45px);
        border-radius: 24px;
        z-index: 1;
    }
    .content-box {
        width: fit-content;
    }
    .btn-box {
        display: flex;
        padding-bottom: 32px;
        border-bottom: 1px solid #a6b1d3;
        margin-bottom: 32px;
    }
    .btn-box .icon-btn {
        font-size: 24px;
        color: #fff;
        text-align: center;
        padding: 24px 48px;
        background: #002640;
        border: 1px solid #047ed1;
        border-radius: 8px;
        box-shadow: 5px 5px 15px rgba(1, 10, 16, 0.8), -5px -5px 15px rgba(9, 87, 178, 0.6);
        margin-right: 16px;
    }
    .btn-box .icon-btn i {
        display: block;
        margin-bottom: 8px;
    }
    .list-btn {
        position: relative;
        width: 100%;
        display: flex;
        justify-content: space-between;
        padding: 16px 48px;
        font-size: 24px;
        color: #fff;
        background: #002640;
        border: 1px solid #047ed1;
        border-radius: 8px;
        box-shadow: 5px 5px 15px rgba(1, 10, 16, 0.8), -5px -5px 15px rgba(9, 87, 178, 0.6);
        margin-bottom: 16px;
    }
    .list-btn .news-dot {
        display: none;
        position: absolute;
        width: 8px;
        height: 8px;
        background: #ca5f56;
        border-radius: 50%;
        top: 50%;
        left: 24px;
        transform: translateY(-50%);
    }
    .list-btn .has-new {
        display: block!important;
    }
    .list-btn .num {
        color: #7cc3ea;
        font-weight: bold;
    }
    .list-btn i {
        display: inline-block;
        margin-left: 48px;
    }
    .d-flex {
        display: flex;
    }
    .p-0 {
        padding: 0;
    }
    .mb-3 {
        margin-bottom: 16px!important;
    }
    .chat-room {
        position: fixed;
        bottom: 0px;
        right: 0px;
    }
</style>

{{-- <div class="row">
    <div class="col-md-3 chat-room">
        <div id="app">
            <div class="box box-primary direct-chat direct-chat-primary" style="margin:0">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('dashboard.customerService') }}</h3>

<div class="box-tools pull-right">
    <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 New Messages">3</span>
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Contacts">
        <i class="fa fa-comments"></i></button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
</div>
</div>
<!-- /.box-header -->
<div class="box-body" style="">
    <!-- 註解：使用template來當迴圈容器，或是判斷用的容器，當條件達成時產出template內容 -->
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages" id="js-roomBody">
        <template v-for="item in messages">
            <!-- other people -->
            <template v-if="item.userName != userName">
                <!-- Message. Default to the left -->
                <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">@{{item.userName}}</span>
                        <span class="direct-chat-timestamp pull-right">@{{item.timeStamp}}</span>
                    </div>
                    <!-- /.direct-chat-info -->
                    <img class="direct-chat-img" src="http://fakeimg.pl/40x40/" alt="Message User Image"><!-- /.direct-chat-img -->
                    <div v-if="item.type == 'text'" class="messageBox__message">
                        <div class="direct-chat-text">
                            @{{item.message}}
                        </div>
                    </div>
                    <div v-if="item.type == 'image'" class="messageBox__image"><img :src="item.message"></div>
                    <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
            </template>
            <!-- 區塊：self -->
            <template v-if="item.userName == userName">
                <!-- Message to the right -->
                <div class="direct-chat-msg right">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">@{{item.userName}}</span>
                        <span class="direct-chat-timestamp pull-left">@{{item.timeStamp}}</span>
                    </div>
                    <!-- /.direct-chat-info -->
                    <img class="direct-chat-img" src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" alt="Message User Image"><!-- /.direct-chat-img -->
                    <div v-if="item.type == 'text'" class="messageBox__message">
                        <div class="direct-chat-text">
                            @{{item.message}}
                        </div>
                    </div>
                    <div v-if="item.type == 'image'" class="messageBox__image"><img :src="item.message"></div>
                    <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
            </template>
        </template>
        <!-- 區塊：上傳進度條 -->
        <div v-show="upload" class="messageBox messageBox--self">
            <div class="messageBox__progress">
                <div id="js-progressBar" class="messageBox__progress--state"></div>
                <div class="messageBox__progress--number">@{{progress}}</div>
            </div>
        </div>
    </div>
    <!--/.direct-chat-messages-->

    <!-- Contacts are loaded here -->
    <div class="direct-chat-contacts">
        <ul class="contacts-list">
            <li>
                <a href="#">
                    <img class="contacts-list-img" src="http://fakeimg.pl/128x128/" alt="User Image">

                    <div class="contacts-list-info">
                        <span class="contacts-list-name">
                            Count Dracula
                            <small class="contacts-list-date pull-right">2/28/2015</small>
                        </span>
                        <span class="contacts-list-msg">How have you been? I was...</span>
                    </div>
                    <!-- /.contacts-list-info -->
                </a>
            </li>
            <!-- End Contact Item -->
        </ul>
        <!-- /.contatcts-list -->
    </div>
    <!-- /.direct-chat-pane -->
</div>
<!-- /.box-body -->
<div class="box-footer" style="">

    <div class="input-group">
        <input type="text" name="message" placeholder="Type Message ..." id="js-message" class="form-control" @keydown.enter="sendMessage($event)">

        <span class="input-group-btn">
            <span class="btn btn-primary" id="fileUpload">
                +<input type="file" id="myInput" accept="image/*" style="display:none" @change="sendImage($event)">
            </span>
            <button type="button" class="btn btn-primary btn-flat" @click="sendMessage()">Send</button>
        </span>
    </div>
</div>
<!-- /.box-footer-->
</div>
</div>

</div>
</div> --}}


@endsection

@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ79gLT1qaLfNJDf98OJr8cT5FZ4MJ4_w&libraries=geometry,places&ext=.js"></script>
<script type="text/javascript" src="https://aishek.github.io/jquery-animateNumber/javascripts/jquery.animateNumber.js"></script>
<script>
    // console.log('{{$viewData["dashboardReadList"]}}');
    let dashboardReadListStr = ('{{$viewData["dashboardReadList"]}}').replace(/&quot;/ig,'"');
    // console.log(dashboardReadListStr);
    let dashboardReadList = dashboardReadListStr ? JSON.parse(dashboardReadListStr) : [];
    let myapplicationFormIds = JSON.parse(('{{json_encode($viewData["myapplicationFormIds"])}}').replace(/&quot;/ig,'"'));
    let myapplicationFormIdsLength = parseInt('{{$viewData["myapplicationForm"]}}');
    let mymaterialsIds = JSON.parse(('{{json_encode($viewData["mymaterialsIds"])}}').replace(/&quot;/ig,'"'));
    let mymaterialsIdsLength = parseInt('{{$viewData["mymaterials"]}}');
    let expiringmaterialIds = JSON.parse(('{{json_encode($viewData["expiringmaterialIds"])}}').replace(/&quot;/ig,'"'));
    let expiringmaterialIdsLength = parseInt('{{$viewData["expiringmaterial"]}}');
    let pendingdeliveryIds = JSON.parse(('{{json_encode($viewData["pendingdeliveryIds"])}}').replace(/&quot;/ig,'"'));
    let pendingdeliveryIdsLength = parseInt('{{$viewData["pendingdelivery"]}}');
    let inventoryprofileIds = JSON.parse(('{{json_encode($viewData["inventoryprofileIds"])}}').replace(/&quot;/ig,'"'));
    let inventoryprofileIdsLength = parseInt('{{$viewData["inventoryprofile"]}}');
    let keeperpendingapplicationIds = JSON.parse(('{{json_encode($viewData["keeperpendingapplicationIds"])}}').replace(/&quot;/ig,'"'));
    let keeperpendingapplicationIdsLength = parseInt('{{$viewData["keeperpendingapplication"]}}');
    let approverpendingapplicationIds = JSON.parse(('{{json_encode($viewData["approverpendingapplicationIds"])}}').replace(/&quot;/ig,'"'));
    let approverpendingapplicationIdsLength = parseInt('{{$viewData["approverpendingapplication"]}}');
    // console.log(dashboardReadList);

    checkHasNewEvent('myapplicationForm', myapplicationFormIds, myapplicationFormIdsLength, 1);
    checkHasNewEvent('mymaterials', mymaterialsIds, mymaterialsIdsLength, 2);
    checkHasNewEvent('expiringmaterial', expiringmaterialIds, expiringmaterialIdsLength, 3);
    checkHasNewEvent('pendingdelivery', pendingdeliveryIds, pendingdeliveryIdsLength, 4);
    checkHasNewEvent('inventoryprofile', inventoryprofileIds, inventoryprofileIdsLength, 5);
    checkHasNewEvent('keeperpendingapplication', keeperpendingapplicationIds, keeperpendingapplicationIdsLength, 6);
    checkHasNewEvent('approverpendingapplication', approverpendingapplicationIds, approverpendingapplicationIdsLength, 7);

    function inventoryprofile() {
        saveDashboardReadListEvent('inventoryprofile');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/listtitleName';
        location.href = redirectUrl;
    }
    function applicationFormbykeeper() {
        saveDashboardReadListEvent('applicationFormbykeeper');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/applicationFormbykeeper';
        location.href = redirectUrl;
    }
    function applicationFormbyapprover() {
        saveDashboardReadListEvent('applicationFormbyapprover');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/applicationFormbyapprover';
        location.href = redirectUrl;
    }
    function myapplicationForm() {
        saveDashboardReadListEvent('myapplicationForm');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/applicationForm';
        location.href = redirectUrl;
    }
    function borrowurl() {
        saveDashboardReadListEvent('borrowurl');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/materialoverview';
        location.href = redirectUrl;
    }
    function mymaterialsurl() {
        saveDashboardReadListEvent('mymaterialsurl');
        var redirectUrl = BASE_URL + '/{{app()->getLocale()}}' + '/mymaterials';
        location.href = redirectUrl;
    }
    var byDayChart = null;
    var byYearChart = null;
    var orderbyDayChart = null;
    var orderbyMonthChart = null;
    var cardata = "<option value=''> </option>";
    var area = 0;

    function checkHasNewEvent (item, itemIds, itemLength, hasNewNum) {
        let hasNew = false;
        console.log(item);
        console.log(itemIds);
        console.log(itemLength);
        console.log(hasNewNum);
        console.log(dashboardReadList[0][item]);
        if (dashboardReadList.length > 0) {
            // 比對使用者已讀的項目，如果有不同顯示紅點
            if (itemLength > 0) {
                if (dashboardReadList[0][item]) {
                    let readLengrh = dashboardReadList[0][item].length
                    let num = 0;
                    for (let i = 0; i < itemLength; i++) {
                        for (let index = 0; index < readLengrh; index++) {
                            if (dashboardReadList[0][item][index] == itemIds[i]) {
                                num += 1
                            }
                        }
                    }
                    if (num < itemLength) {
                        hasNew = true;
                    }
                } else {
                    hasNew = true;
                }
            }
        } else {
            if (itemLength > 0) {
                hasNew = true;
            }
        }
        if (hasNew) {
            if ((item !== 'keeperpendingapplication' && item !== 'approverpendingapplication') || (item === 'keeperpendingapplication' && '{{$viewData["iskeeper"]}}' === 'Y') || (item === 'approverpendingapplication' && '{{$viewData["isapprover"]}}' === 'Y')) {
                document.getElementById(`newsDot-${hasNewNum}`).classList.add('has-new');
            }
        }
    }
    function saveDashboardReadListEvent (item) {
        console.log('儲存');
        let lang = "en";
        let userId = "";
        @if(isset($viewData["lang"]))
            lang   = '{{$viewData["lang"]}}';
        @endif
        @if(isset($viewData["userId"]))
            userId   = '{{$viewData["userId"]}}';
        @endif
        $.ajax({
            url: "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/set/dashboard/read/list",
            type: 'POST',
            data: {
                dashboardReadList: JSON.stringify([{
                    "myapplicationForm": item === 'myapplicationForm' ? myapplicationFormIds : (dashboardReadList[0]?.myapplicationForm || []),
                    "mymaterials": item === 'mymaterialsurl' ? mymaterialsIds : (dashboardReadList[0]?.mymaterials || []),
                    "expiringmaterial": item === 'mymaterialsurl' ? expiringmaterialIds : (dashboardReadList[0]?.expiringmaterial || []),
                    "pendingdelivery": item === 'myapplicationForm' ? pendingdeliveryIds : (dashboardReadList[0]?.pendingdelivery || []),
                    "inventoryprofile": item === 'inventoryprofile' ? inventoryprofileIds : (dashboardReadList[0]?.inventoryprofile || []),
                    "keeperpendingapplication": item === 'applicationFormbykeeper' ? keeperpendingapplicationIds : (dashboardReadList[0]?.keeperpendingapplication || []),
                    "approverpendingapplication": item === 'applicationFormbyapprover' ? approverpendingapplicationIds : (dashboardReadList[0]?.approverpendingapplication || [])
                }])
            },
            beforeSend: function () {
                loadingFunc.show();
            },
            success: function (data) {
                console.log(data);
                loadingFunc.hide();
                return false;
            }
        });
    }
</script>
<!-- <script src="{{ asset('js') }}/main.js?v={{Config::get('app.version')}}"></script> -->

@endsection
<!doctype html>

<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>照片</title>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

</head>

<body>
    <img src=" {{ url('storage/img/'.$imgData)}}" id="imgsize"  onclick="sizechang('imgsize')" class="img-fluid" alt="Responsive image" width="300">
</body>
<script>
    @if(isset($imgtype))
    var type = "{{$imgtype}}";
    @endif
function sizechang(id) {
    
    if(document.getElementById(id).width == "300"){
        document.getElementById(id).width ="800";
    }else{
        document.getElementById(id).width ="300";
    }
}
$(window).on("beforeunload", function() { 
    if(type=="sign"){
        window.opener.document.getElementById("updategridsign").click();
    }else{
        window.opener.document.getElementById("updategriderror").click();
    }
})

</script>
</html>
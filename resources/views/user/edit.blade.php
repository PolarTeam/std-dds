@extends('layout.layout')

@section('header')
    <section class="content-header">
    <input type="button" style="display:none" id="updategrid" >
      <h1>
        {{ trans('users.titleName') }}<small></small>
      </h1>
      <ol class="breadcrumb">        
        <li><a href="{{ url(config('backpack.base.route_prefix', '') . '/user') }}">{{ trans('users.titleName') }}</a></li>
		<li class="active">{{ trans('users.titleName') }}</li>
      </ol>
    </section>
@endsection

@section('content')
@include('layout.template.toolbar')

<div class="row">
    
    <div class="col-md-12">
        <div class="callout callout-danger" id="errorMsg" style="display:none"> 
            <h4>{{ trans('backpack::crud.please_fix') }}</h4> 
            <ul> 
            
            </ul> 
        </div> 
        <form method="POST"  accept-charset="UTF-8" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <form role="form">
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <input type="hidden" class="form-control" id="session_id" name="session_id">
                                        <label for="name">{{ trans('users.name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">{{ trans('users.email') }}</label>
                                        <input type="text" class="form-control" id="email" name="email" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password">{{ trans('users.password') }}</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="password_confirmation">{{ trans('users.passwordConfirmation') }}</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="status">{{ trans('users.role') }}</label>
                                        <select class="form-control select2" id="role" name="role" multiple="multiple">
                                            @foreach($role as $row)
                                            <option value="{{$row->name}}">{{$row->display_name}}</option>
                                            @endforeach	
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="isEnabled">{{ trans('users.accountEnable') }}</label>
                                        <select class="form-control" id="account_enable" name="account_enable">
                                            <option value="Y">{{trans('common.yes')}}</option>
                                            <option value="N">{{trans('common.no')}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.gKey') }}</label>
                                        <input type="text" class="form-control" id="g_key" name="g_key" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.cKey') }}</label>
                                        <input type="text" class="form-control" id="c_key" name="c_key" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.sKey') }}</label>
                                        <input type="text" class="form-control" id="s_key" name="s_key" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ trans('users.dKey') }}</label>
                                        <input type="text" class="form-control" id="d_key" name="d_key" required="required">
                                    </div>
                                </div>
                                <div class="row"> 
                                    @if(isset($id))
                                        <input style="display: none" type="text" id="created_at" name="created_at" disabled="disabled">
                                        <input style="display: none" type="text" id="updated_at" name="updated_at" disabled="disabled">
                                        <input type="hidden" name="id" value="{{$id}}" class="form-control">
                                        <input type="hidden" name="_method" value="PUT" class="form-control">
                                    @endif
                                </div>
                        </form>
                    </div>

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </form>
    </div> 
</div>


{{-- <div class="row">
	<div class="col-md-12">
        <div class="nav-tabs-custom" @if(!isset($id)) style="display:none" @endif id="subPanel">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_2" data-toggle="tab" aria-expanded="false">使用者權限</a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    <div class="box box-primary" id="subBox" style="display:none">
                        <div class="box-header with-border">
                        <h3 class="box-title">使用者權限</h3>
                        </div>
                        <form method="POST"  accept-charset="UTF-8" id="subForm" enctype="multipart/form-data">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="cd">{{ trans('bscode.cd') }}</label>
                                    <input type="text" class="form-control input-sm" name="cd" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cd_descp">{{ trans('bscode.cdDescp') }}</label>
                                    <input type="text" class="form-control input-sm" name="cd_descp" grid="true" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="value1">{{ trans('bscode.value1') }}</label>
                                    <input type="text" class="form-control input-sm" name="value1" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value2">{{ trans('bscode.value2') }}</label>
                                    <input type="text" class="form-control input-sm" name="value2" grid="true" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="value3">{{ trans('bscode.value3') }}</label>
                                    <input type="text" class="form-control input-sm" name="value3" grid="true" >
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="hidden" class="form-control input-sm" name="id" grid="true" >
                            <button type="button" class="btn btn-sm btn-primary" id="Save">{{ trans('common.save') }}</button>
                            <button type="button" class="btn btn-sm btn-danger" id="Cancel">{{ trans('common.cancel') }}</button>
                        </div>
                        </button>
                    </div>
                    
                    <div id="jqxGrid"></div>
                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
	</div>
	
</div> --}}

@endsection
@include('layout.template.lookup')

@section('after_scripts')


<script>
    var mainId     = "";
    var lang       = "en";
    var cust_no    = "";
    var editData   = null;
    var editObj    = null;
    var fieldData  = null;
    var fieldObj   = null;


    
    @if(isset($id))
        mainId = "{{$id}}";
    @endif

    @if(isset($lang))
        lang   = "{{$lang}}";
    @endif

    var SAVE_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
    var CREATE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/create"
    var EDIT_URL   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
    var DELETE_URL = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user"
   

    $(function(){

        $('#myForm button[btnName="manage_name"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('manage_name', "管理者搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="manage_name"]').on('click', function(){
            var check = $('#subBox input[name="manage_name"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","manage_name",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_name");
            }
        });

        $('#myForm button[btnName="dep"]').on('click', function(){
            $('#lookupModal').modal('show');
            initLookup('dep', "部門搜尋", callBackFunc=function(data){
                console.log(data);
            });
        });

        $('#myForm input[name="dep"]').on('click', function(){
            var check = $('#subBox input[name="dep"]').data('ui-autocomplete') != undefined;
            if(check == false) {
                initAutocomplete("myForm","dep",callBackFunc=function(data){
                    console.log(data);
                    
                },"manage_depname");
            }
        });

        //var formOpt = {};
        formOpt.formId    = "myForm";
        formOpt.editObj   = editObj;
        formOpt.fieldObj  = fieldObj;
        formOpt.editUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/" + mainId;
        formOpt.fieldsUrl = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user/" + mainId;
        formOpt.saveUrl   = "{{ url(config('backpack.base.route_prefix', ''))}}" +"/"+lang+"/user";
        
        formOpt.initFieldCustomFunc = function (){
            // console.log('test step');
            $('#role').select2();
            
        };
        formOpt.addFunc = function() {
            $('#role').val();
            $('#role').trigger('change');
        }

        formOpt.copyFunc = function() {

        }
        formOpt.afterInit = function() {
            console.log($('#role').val());
        }
        
        formOpt.beforesaveFunc = function() {
            var iserror = false ;

            if($('#name').val() == "") {
                document.getElementById("name").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("name").style.backgroundColor = "";
            }

            if($('#email').val() == "") {
                document.getElementById("email").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("email").style.backgroundColor = "";
            }

            if($('#g_key').val() == "") {
                document.getElementById("g_key").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("g_key").style.backgroundColor = "";
            }

            if($('#c_key').val() == "") {
                document.getElementById("c_key").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("c_key").style.backgroundColor = "";
            }

            if($('#s_key').val() == "") {
                document.getElementById("s_key").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("s_key").style.backgroundColor = "";
            }

            if($('#d_key').val() == "") {
                document.getElementById("d_key").style.backgroundColor = "FCE8E6";
                iserror = true;
            } else {
                document.getElementById("d_key").style.backgroundColor = "";
            }


            if($('#password').val() != "" ) {
                if( $('#password').val() !=  $('#password_confirmation').val()) {
                    swal("{{trans('common.warning')}}", "{{trans('common.passwordnotmatch')}}", "warning");
                    return false;
                }
            }
            if( mainId == "" && $('#password').val() == "") {
                document.getElementById("password").style.backgroundColor = "FCE8E6";
                iserror = true;
            }

            if(iserror) {
                swal("{{trans('common.warning')}}", "{{trans('common.requiredfields')}}", "warning");
                return false;
            }

            if($('#role').val() == null) {
                swal("{{trans('common.warning')}}", "{{trans('common.rolenotempty')}}", "warning");
                return false;
            }
            return true;
        }
        setField.disabled("myForm",["created_by","created_at","updated_by","updated_at"]);


    })
</script> 
<script>
$(function(){

});
</script>
@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>{{env('APP_NAME')}}客服查詢</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/pricing/">

    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
</head>

<body>

        <div class="row" style="margin-left: 10px;">
        {{env('APP_NAME')}}客服查詢
        </div>

        <form class="row g-3" method="get">
            <div class="col-auto">
                <select class="form-select" aria-label="Default select example" name="queryType">
                    <option selected>請選擇</option>
                    <option value="inbound">入庫</option>
                    <option value="outbound">出庫</option>
                </select>
            </div>
            <div class="col-auto">
                <input type="text" class="form-control" id="refNo" placeholder="inbound no or outbound no" name="refNo">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3">Confirm</button>
            </div>
        </form>

        <table class="table table-bordered" style="margin-top: 10px; font-size:10px">
            <thead>
                <tr>
                    <th>動作</th>
                    <th>post data</th>
                    <th>操作人</th>
                    <th>發生時間</th>
                </tr>
            </thead>
            <tbody>
                @foreach($logs as $key=>$row)
                <tr>
                    <td>{{isset($row['subject']) ? $row['subject'] : ""}}</td>
                    <td style="word-break: break-all;">{{isset($row['post_data']) ? $row['post_data'] : ""}}</td>
                    <td>{{isset($row['created_by_name'])? $row['created_by_name'] : ""}}</td>
                    <td>{{isset($row['created_at'])? $row['created_at'] : ""}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <footer class="pt-4 my-md-5 pt-md-5 border-top">
        </footer>

</body>

</html>
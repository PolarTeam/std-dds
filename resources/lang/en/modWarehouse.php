<?php 

 return [
    "id"             => "id",
    "storageName"    => "Storag",
    "vendorAttn"     => "Attn",
    "name"           => "Name",
    "cdType"         => "Name",
    "type"           => "Type",
    "sorted"         => "Sort",
    "baseinfo"       => "Basic Information",
    "titleName"      => "Warehouse",
    "displayName"    => "Display Name",
    "createdAt"      => "Created At",
    "updatedAt"      => "Updated At",
    "code"           => "Code",
    "floorHouseName" => "Floor House Name",
    "shelfNo"        => "Shelf No",
    "layer"          => "Layer",
    "remark"         => "Remark",
    "createdByName" => "Created Name",
    "updatedByName" => "Updated Name",
];
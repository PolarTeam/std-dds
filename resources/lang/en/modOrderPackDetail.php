<?php 

 return [
    "createdAt" => "Created Date",
    "createdBy" => "Created User",
    "ordDetailId" => "Order Detail Id",
    "ordNo" => "Order No.",
    "packNo" => "Packing No.",
    "packingContent" => "Packing Content",
    "updatedAt" => "Updated Time",
    "updatedBy" => "Updated User",
    "num" => "Want To Packing Quantity",
    "id" => "id"
];
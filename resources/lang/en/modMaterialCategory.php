<?php 

 return [
    "id"            => "id",
    "materialType"  => "Material Category",
    "partName"      => "part Name",
    "status"        => "status",
    "cdType"        => "Name",
    "type"          => "Type",
    "sorted"        => "Sort",
    "baseinfo"      => "Basic Information",
    "titleName"     => "Material Category",
    "displayName"   => "Display Name",
    "createdAt"     => "Created At",
    "updatedAt"     => "Updated At",
    "createdByName" => "Created Name",
    "updatedByName" => "Updated Name",
    "shorthand"     => "Shorthand",
    "finalBarcode"  => "Barcode Now",
];
<?php 

 return [
    "id"            => "id",
    "title"         => "Subject",
    "updatedBy"     => "Updated By",
    "updatedAt"     => "Updated Time",
    "createdBy"     => "Created By",
    "createdAt"     => "Created Time",
    "content"       => "Content",
    "titleName"     => "Mail Format",
    "createdByName" => "Created By",
    "updatedByName" => "Updated By",
    "type"          => "Type",
    "typeDescp"     => "Mail format",
    "name"          => "Name",
];
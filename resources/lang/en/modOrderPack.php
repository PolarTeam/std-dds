<?php 

 return [
    "id" => "ID",
    "goodsTnm" => "商品統稱",
    "weight" => "寬度",
    "createdBy" => "建單人員",
    "createdAt" => "建立時間",
    "updatedBy" => "最後修改人員",
    "updatedAt" => "最後更新時間",
    "cbm" => "材/體積",
    "cbmu" => "材/體積單位",
    "cntrNo" => "櫃號",
    "cntrMaxWeight" => "箱型限重",
    "totalNum" => "總件數",
    "gw" => "總重量",
    "gwu" => "總重量單位",
    "packNo" => "裝箱編號",
    "loadComp" => "裝載完畢",
    "ordNo" => "訂單號碼",
    "length" => "長度",
    "height" => "高度"
];
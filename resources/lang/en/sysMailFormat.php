<?php 

return [
    "id" => "id",
    "mType" => "郵件類別代碼",
    "mName" => "郵件類別",
    "mContent" => "郵件格式",
    "gKey" => "Group",
    "cKey" => "Company",
    "sKey" => "Station",
    "dKey" => "Dept.",
    "createdAt" => "Created Time",
    "updatedAt" => "Updated Time",
    "updatedBy" => "Updated User",
    "createdBy" => "Created User",
    ];
<?php 

 return [
    "id"            => "id",
    "vendorNo"      => "Vendor No",
    "vendorName"    => "Name",
    "vendorAddress" => "Address",
    "vendorAttn"    => "Attn",
    "name"          => "Name",
    "cdType"        => "Name",
    "type"          => "Type",
    "sorted"        => "Sort",
    "baseinfo"      => "Basic Information",
    "titleName"     => "Vendor",
    "displayName"   => "Display Name",
    "createdAt"     => "Created At",
    "updatedAt"     => "Updated At",
    "createdByName" => "Created Name",
    "updatedByName" => "Updated Name",
];
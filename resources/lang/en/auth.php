<?php 

 return [
    "email" => "Email address not found",
    "password" => "Incorrect password",
    "failed" => "These credentials do not match our records.",
    "throttle" => "Too many login attempts. Please try again in :seconds seconds.",
    "identity" => "ID",
    "sKey" => "Station"
];
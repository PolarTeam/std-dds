<?php 

 return [
    "id" => "id",
    "updatedByName"=>"Updated By",
    "createdByName"=>"Created By",
    "baseInfo" => "Information",
    "cdDescp" => "Description",
    "titleName"   => "Basic",
    "cd" => "Code",
    "detail" => "Detail",
    "cdType" => "Type",
    "updatedBy" => "Updated User",
    "updatedAt" => "Update Time",
    "cKey" => "Company",
    "createdBy" => "Created User",
    "createdAt" => "Created Time",
    "sKey" => "Station",
    "dKey" => "Dept.",
    "gKey" => "Group",
    "adminonly" => 'Show',
    "titleAddName" => "Basic Code"
];
<?php 

 return [
    "id"               => "id",
    "partNo"           => "part No",
    "partName"         => "part Name",
    "status"           => "status",
    "cdType"           => "Name",
    "type"             => "Type",
    "sorted"           => "Sort",
    "baseinfo"         => "Basic Information",
    "titleName"        => "Wind Chil ",
    "displayName"      => "Display Name",
    "createdAt"        => "Created At",
    "updatedAt"        => "Updated At",
    "createdByName" => "Created Name",
    "updatedByName" => "Updated Name",
];
<?php 

 return [
    "id"          => "id",
    "funcName"    => "Code",
    "enName"      => "English title",
    "twName"      => "Chinese title",
    "type"        => "Type",
    "sorted"      => "Sort",
    "baseinfo"    => "Basic Information",
    "titleName"   => "Language Management",
    "displayName" => "Display Name",
    "createdAt"   => "Created At",
    "updatedAt"   => "Updated At",
];
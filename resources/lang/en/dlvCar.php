<?php 

 return [
    "cancel" => "Cancel",
    "driver" => "Driver",
    "selected" => "Select Volume",
    "selectedRate" => "Select Loading Rate",
    "msg2" => "您所選擇的訂單已超過車輛裝載限制",
    "msg1" => "您所選的材積已經超過該輛車的總材積！！",
    "msg3" => "您是否要強制裝載？",
    "phone" => "Mobile",
    "titleName" => "Arrangement",
    "insertOrd" => "Insert",
    "yes" => "Yes",
    "deliveryWork" => "派車作業",
    "title" => "派車作業",
    "sendCarDate" => "派車日期",
    "clear" => "Clear",
    "confirmSendCar" => "Confirm",
    "loadingRate" => "Loading Rate",
    "carDealer" => "Vendor",
    "carType" => "Type",
    "carCapacity" => "Capacity",
    "carNo" => "Car No.",
    "car" => "Car",
    "tranPlan" => "Transportation Plan",
    "isUrgent" => "Urgent"
];
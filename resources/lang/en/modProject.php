<?php 

 return [
    "id"               => "id",
    "projectName"      => "Name",
    "manageByName"     => "Manage",
    "manageName"       => "Manage",
    "secondManageName" => "Second Manage",
    "secondManage"     => "Second Manage",
    "cdType"           => "Name",
    "type"             => "Type",
    "sorted"           => "Sort",
    "baseinfo"         => "Basic Information",
    "titleName"        => "Project",
    "displayName"      => "Display Name",
    "createdAt"        => "Created At",
    "updatedAt"        => "Updated At",
    "createdByName" => "Created Name",
    "updatedByName" => "Updated Name",
];
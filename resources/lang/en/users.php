<?php 

 return [
    "id"                   => "id",
    "name"                 => "Name",
    "email"                => "Account",
    "password"             => "Incorrect Password",
    "passwordConfirmation" => "Password Confirmation",
    "manageName"           => "Management Supervisor",
    "failed"               => "These credentials do not match our records.",
    "throttle"             => "Too many login attempts. Please try again in :seconds seconds.",
    "identity"             => "Identity",
    "role"                 => "Role",
    "createdAt"            => "Created At",
    "updatedAt"            => "Updated At",
    "titleName"            => "User Management",
    "sKey"                 => "Station Key",
];
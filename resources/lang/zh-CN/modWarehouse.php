<?php 

 return [
    "id"               => "id",
    "storageName"      => "储位",
    "name"             => "名称",
    "code"             => "代码",
    "floorHouseName"   => "楼层房名",
    "shelfNo"          => "货架编号",
    "layer"            => "层",
    "remark"           => "备注",
    "baseinfo"         => "基本资讯",
    "titleName"        => "储位建档",
    "createdAt"        => "建立时间",
    "updatedAt"        => "修改时间",
    "updatedByName"    => "修改人",
    "createdByName"    => "创建人",
];
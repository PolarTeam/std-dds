<?php 

return [
    "id" => "id",
    "cdType" => "cdType",
    "cdDescp" => "cdDescp",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdBy" => "createdBy",
    "updatedBy" => "updatedBy",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    ];
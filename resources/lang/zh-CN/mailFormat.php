<?php 

return [
    "id" => "id",
    "type" => "type",
    "name" => "name",
    "content" => "content",
    "title" => "title",
    "gKey" => "gKey",
    "cKey" => "cKey",
    "sKey" => "sKey",
    "dKey" => "dKey",
    "createdBy" => "createdBy",
    "updatedBy" => "updatedBy",
    "createdAt" => "createdAt",
    "updatedAt" => "updatedAt",
    ];
<?php 

 return [
    "cKey" => "公司",
    "dKey" => "部門",
    "description" => "描述",
    "gKey" => "集團",
    "id" => "id",
    "isRelieve" => "是否解除",
    "reason" => "異常原因",
    "sKey" => "站別",
    "type" => "代碼類別"
];
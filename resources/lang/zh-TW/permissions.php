<?php 

 return [
    "id"          => "id",
    "Id"          => "id",
    "name"        => "名稱",
    "type"        => "類型",
    "sorted"      => "排序",
    "baseinfo"    => "基本資訊",
    "titleName"   => "權限建檔",
    "displayName" => "顯示名稱",
    "createdAt"   => "建立時間",
    "updatedAt"   => "修改時間",
];
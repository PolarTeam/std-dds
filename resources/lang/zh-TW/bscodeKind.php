<?php 

 return [
    "id"            => "id",
    "updatedByName" => "修改人",
    "createdByName" => "創建人",
    "baseInfo"      => "基本資訊",
    "cdDescp"       => "代碼類別",
    "titleName"     => "基本代碼建檔",
    "cd"            => "代碼",
    "detail"        => "代碼明細資料",
    "cdType"        => "代碼名稱",
    "updatedBy"     => "修改人",
    "updatedAt"     => "修改時間",
    "createdBy"     => "建立人",
    "createdAt"     => "建立時間",
    "gKey"          => "集團",
    "adminonly"     => '顯示',
];
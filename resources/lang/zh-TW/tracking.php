<?php 

 return [
    "spiltOrdno" => "多張訂單號請用半形逗號隔開",
    "msg1" => "查無此訂單號",
    "result" => "查詢結果",
    "ordNo" => "訂單號碼",
    "msg2" => "請確認訂單號碼是否正確",
    "pleaseInputOrdno" => "請輸入訂單號碼",
    "pleaseInputOrder" => "請輸入訂單號碼...",
    "tracking" => "貨況追蹤",
    "inputOrdNo" => "輸入訂單號碼",
    "titleName" => "追踨訂單",
    "chooseOrder" => "選擇單號",
    "prompt" => "提示"
];
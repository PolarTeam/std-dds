<?php 

 return [
    "id"          => "id",
    "name"        => "名稱",
    "baseinfo"    => "基本資訊",
    "titleName"   => "角色建檔",
    "displayName" => "顯示名稱",
    "createdAt"   => "建立時間",
    "updatedAt"   => "修改時間",
];
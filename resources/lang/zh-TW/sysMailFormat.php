<?php 

return [
    "id" => "id",
    "mType" => "郵件類別代碼",
    "mName" => "郵件類別",
    "mContent" => "郵件格式",
    "gKey" => "集團",
    "cKey" => "公司",
    "sKey" => "站別",
    "dKey" => "部門",
    "createdAt" => "創建時間",
    "updatedAt" => "修改時間",
    "updatedBy" => "修改人",
    "createdBy" => "創建人",
    ];
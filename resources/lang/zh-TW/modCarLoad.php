<?php 

 return [
    "coorX" => "擺放位置X",
    "coorY" => "擺放位置Y",
    "coorZ" => "擺放位置Z",
    "createdAt" => "建單時間",
    "createdBy" => "建單人員",
    "dlvNo" => "Delivery No",
    "ordNo" => "Order No",
    "packNo" => "Pack No",
    "sort" => "Sort",
    "updatedAt" => "最後修改時間",
    "updatedBy" => "最後修改時間",
    "id" => "id"
];
<?php 

 return [
    "id"               => "id",
    "partNo"           => "料品編號",
    "partName"         => "料品名稱",
    "status"           => "狀態",
    "baseinfo"         => "基本資訊",
    "titleName"        => "Wind Chil ",
    "createdAt"        => "建立時間",
    "updatedAt"        => "修改時間",
    "updatedByName"    => "修改人",
    "createdByName"    => "創建人",
];
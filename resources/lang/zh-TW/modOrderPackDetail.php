<?php 

 return [
    "createdAt" => "建單時間",
    "createdBy" => "建單人員",
    "ordDetailId" => "Order Detail Id",
    "ordNo" => "訂單號",
    "packNo" => "裝箱編號",
    "packingContent" => "Packing Content",
    "updatedAt" => "最後修改時間",
    "updatedBy" => "最後修改人員",
    "num" => "Want To Packing Quantity",
    "id" => "id"
];
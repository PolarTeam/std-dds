<?php 

 return [
    "id"                   => "id",
    "name"                 => "名稱",
    "email"                => "登入帳號",
    "password"             => "密碼",
    "passwordConfirmation" => "密碼確認",
    "manageName"           => "管理主管",
    "identity"             => "身份",
    "role"                 => "角色",
    "createdAt"            => "創建時間",
    "updatedAt"            => "修改時間",
    "titleName"            => "使用者管理",
    "sKey"                 => "集團",
];
<?php 

 return [
    "id"            => "id",
    "materialType"  => "物料類別",
    "baseinfo"      => "基本資訊",
    "titleName"     => "物料類別",
    "createdAt"     => "建立時間",
    "updatedAt"     => "修改時間",
    "updatedByName" => "修改人",
    "createdByName" => "創建人",
    "shorthand"     => "簡寫",
    "finalBarcode"  => "當前Barcode",
];
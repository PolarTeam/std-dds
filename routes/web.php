<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@loginView')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('admin/baseApi/getGridJson/{table}/{status?}', 'BaseApiController@getGridJson');
Route::get('admin/baseApi/getStatusCount/{tableName}/{langname?}', 'BaseApiController@getStatusCount');
Route::get('admin/baseApi/getFieldsJson/{table}', 'BaseApiController@getFieldsJson');
Route::get('admin/baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
Route::get('admin/baseApi/getinboundbymy/{table}/{status?}/{userid?}', 'BaseApiController@getinboundbymy');
Route::get('admin/baseApi/getinboundbymyall/{table}/{status?}/{userid?}', 'BaseApiController@getinboundbymyall');

Route::get('admin/baseApi/getLookupFieldsJson/{info1}/{info2}/{info5?}/{lang?}', 'BaseApiController@getLookupFieldsJson');
Route::get('admin/baseApi/getLookupGridJson/{info1}/{info2}/{info3}', 'BaseApiController@getLookupGridJson');
Route::get('admin/baseApi/getAutocompleteJson/{info1}/{info2}/{info3}/{input}', 'BaseApiController@getAutocompleteJson');

Route::get('show/public/img/{path?}', 'CommonController@showChkImg');

Route::get('admin/baseApi/getLayoutJson', 'BaseApiController@getLayoutJson');
Route::post('admin/baseApi/saveLayoutJson', 'BaseApiController@saveLayoutJson');
Route::post('admin/baseApi/clearLayout', 'BaseApiController@clearLayout');

Route::get('searchList/get/{key}', 'CommonController@getSearchLayoutList');
Route::get('searchHtml/get/{id?}', 'CommonController@getSearchHtml');
Route::post('userPwd/update', 'CommonController@updatePwd');
Route::post('userlang/change', 'CommonController@changelang');
Route::put('setSearchDefault/{key}/{id}', 'CommonController@setSearchDefault');
Route::DELETE('delSearchLayout/{id}', 'CommonController@delSearchLayout');
Route::group( ['middleware' => ['auth', 'locale'], 'prefix' => '{lang?}' ], function () {

    Route::get('/', function () {
        return redirect('/board');
    });

    Route::post('saveSearchLayout', 'CommonController@saveSearchLayout');
    Route::get('/board', 'DashboardController@dashboard');

    //使用者管理
    Route::resource('user', 'UserMgmtController');
    Route::post('user/batchDelete', 'UserMgmtController@batchDelete');
    Route::post('export/data', 'UserMgmtController@exportdata');
    Route::get('permission/user', 'UserMgmtController@getpermissionUser');
    Route::post('user/query', 'UserMgmtController@query');
    Route::post('user/queryemail', 'UserMgmtController@queryemail');
    Route::post('user/queryemailall', 'UserMgmtController@queryemailall');
    Route::post('user/excel/export', 'UserMgmtController@exportdata');
    Route::post('user/set/dashboard/read/list', 'UserMgmtController@setDashboardReadList');
    //角色
    Route::resource('role', 'RolesController');
    Route::post('role/batchDelete', 'RolesController@batchDelete');
    Route::post('role/excel/export', 'RolesController@exportdata');

    //客戶建檔
    Route::resource('customerProfile', 'CustomerProfileController');
    Route::post('customerProfile/batchDelete', 'CustomerProfileController@batchDelete');
    Route::post('customerProfile/excel/export', 'CustomerProfileController@exportdata');
    Route::post('customerProfile/store', 'CustomerProfileCrudController@feeStore');//服務項目
    Route::post('customerProfile/update', 'CustomerProfileCrudController@feeUpdate');//服務項目
    Route::get('customerProfile/delete/{id}', 'CustomerProfileCrudController@feeDel');//服務項目
    Route::get('customerProfile/get/{cust_id?}', 'CustomerProfileCrudController@feeGet');//服務項目
    Route::post('customerProfile/store', 'CustomerProfileCrudController@errorStore');//異常項目
    Route::post('customerProfile/update', 'CustomerProfileCrudController@errorUpdate');//異常項目
    Route::get('customerProfile/delete/{id}', 'CustomerProfileCrudController@errorDel');//異常項目
    Route::get('customerProfile/get/{cust_id?}', 'CustomerProfileCrudController@errorGet');//異常項目
    

    //權限
    Route::resource('permission', 'PermissionController');
    Route::post('permission/batchDelete', 'PermissionController@batchDelete');
    Route::post('permission/excel/export', 'PermissionController@exportdata');



    //基本健檔
    Route::resource('bscodeKind', 'BscodeKindController');
    Route::post('bscodeKind/batchDelete', 'BscodeKindController@batchDelete');
    Route::post('bscodeKind/excel/export', 'BscodeKindController@exportdata');
    Route::get('bscode/{cd_type?}', 'BscodeKindController@detailget');
    Route::post('bscode/store', 'BscodeKindController@detailStore');
    Route::post('bscode/update/{cd_type}', 'BscodeKindController@detailUpdate');
    Route::get('bscode/delete/{id}', 'BscodeKindController@detailDel');

    //匯入excel
    Route::resource('mapping', 'MappingController');
    Route::post('mapping/batchDelete', 'MappingController@batchDelete');
    Route::post('mapping/excel/export', 'MappingController@exportdata');
    Route::get('mappingDetail/{mapping_code?}', 'MappingController@detailget');
    Route::post('mappingDetail/store', 'MappingController@detailStore');
    Route::post('mappingDetail/update/{mapping_code}', 'MappingController@detailUpdate');
    Route::get('mappingDetail/delete/{id}', 'MappingController@detailDel');

    //多語系建檔
    Route::resource('language', 'languageController');
    Route::post('language/excel/export', 'languageController@exportdata');

    //mailformat
    Route::resource('mailFormat', 'MailFormatController');
    Route::post('mailFormat/batchDelete', 'MailFormatController@batchDelete');
    Route::post('mailFormat/excel/export', 'MailFormatController@exportdata');
    Route::post('mailFormat/send/mail', 'MailFormatController@sendmail');



    //檔案(附件)
    Route::post('tranferuploadfile', 'FileController@tranferuploadfile');
    Route::post('file/upload/{applono?}', 'FileController@uploadfile');
    Route::post('file/delete/{applono?}', 'FileController@deletefile');
    Route::post('file/download/{applono?}', 'FileController@downloadfile');



    //excel匯入範本
    Route::resource('jdsDataImport', 'JdsDataImportController');

    //sample
    Route::resource('sample', 'SampleController');

    Route::get('baseApi/getFunctionSchemaForGrid/{functionName}', 'BaseApiController@getFunctionSchemaForGrid');

});

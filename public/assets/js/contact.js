(function($) {
	"use strict";
	// Contact form valitation with jquery.validate plugin
	if ($.fn.validate) {
        var contactForm = $('#contact-form'),
            formBtn = contactForm.find('.btn');

        contactForm.validate({
            rules: {
                contactname: 'required',
                // contactwebsite: {
                //     required: true,
                //     url: true
                // },
                contactsubject: 'required',
                contactemail: {
                    required: true,
                    email: true
                },
                contactmessage: {
                    required: true,
                    minlength: 40
                }
            },
            messages: {
                contactname: "必填，請輸入您的名稱",
                // contactwebsite: {
                //     required: "This field is required. Please enter your website.",
                //     email: "Please enter a valid url."
                // },
                contactsubject: "必填，請輸入您的主旨",
                contactemail: {
                    required: "必填，請輸入您的 email",
                    email: "格式錯誤"
                },
                contactmessage: {
                    required: "必填，請輸入您的訊息",
                    minlength: "您輸入的文字需小於40個字"
                }
            },
            submitHandler: function (form) {
                event.preventDefault();
                $(document).ajaxStart(function() {
                    formBtn.button('loading');
                }).ajaxStop(function() {
                    formBtn.button('reset');
                });
                /* Ajax handler */
                $.ajax({
					type: 'post',
					url: BASE_URL+'/post/message',
					data: $(form).serialize(),
				}).done(function( data ) {
					if ( data.msg == 'success') {
						alert('已收到您的問題，我們將盡快與您聯絡');
					} else {
						alert('網路似乎發生了問題，請您稍後再試');
                    }
				}).error(function() {
                    alert( '網路似乎發生了問題，請您稍後再試' );
				});
                return false;
            }
        });
    }
})(jQuery);
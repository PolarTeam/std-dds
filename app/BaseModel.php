<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Exception;
use Illuminate\Support\Facades\Log;
use App\AutoNo;
use App\AutoNoT;

class BaseModel extends Model
{

    public function baseGetFiled($table) {
        $query = DB::table('function_schema');
        $query->where('root_id', $table);
        $result = $query->get();

        return $result;
    }
    public function getuserbymail($email) {
        $query = DB::table('users');
        $query->where('email', $email);
        $result = $query->first();

        return $result;
    }
    public function getuserbyName($name) {
        $query = DB::table('users');
        $query->where('name', $name);
        $result = $query->first();

        return $result;
    }
    public function getkeeper() {
        $query = DB::table('users');
        $query->where('role', 'like', '%keeper%');
        $result = $query->pluck('email')->toArray();

        return $result;
    }
    public function getuserbyid($id) {
        $query = DB::table('users');
        $query->where('id', $id);
        $result = $query->first();

        return $result;
    }

    public function getuserbyidmulti($id) {
        $query = DB::table('users');
        $query->whereIn('id', $id);
        $result = $query->pluck('email')->toArray();

        return $result;
    }

    public function getuserbyids($ids) {
        $query = DB::table('users');
        $query->whereiN('id', $ids);
        $result = $query->pluck('email')->toArray();
        return $result;
    }

    public function getUserNameByids($userIds) {
        $query = DB::table('users');
        $query->whereIn('id', $userIds);
        $result = $query->pluck('name')->toArray();

        return $result;
    }
    public function baseQuery($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $subSelect=array()) {

        // array_push($baseCondition, [
        //     'is_deleted', '=', 0
        // ]);

        // DB::listen(function ($query){
        //     Log::info($query->sql);
        //     Log::info($query->bindings);
        // });

        $query = DB::table($table);

        if ( is_array($subSelect) && count($subSelect) > 0) {
            $query->addselect('*');
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            // Log::info($baseCondition);

            foreach($baseCondition as $condition) {
                if( is_array($condition) && count($condition)) {
                    if($condition[1] === 'in') {
                        $query->whereIn($condition[0], $condition[2]);
                    }
                    else {
                        $query->where($condition[0], $condition[1], $condition[2]);
                    }
                }
            }
            
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        $result = $query->take($pageSize)->skip($startNum)->get();

        $bindings = $query->getBindings();
        $sql = str_replace('?', '%s', $query->toSql());
        $sql = sprintf($sql, ...$bindings);
        Log::info($sql);

        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query '.$table.' success',
            'data' => $result
        );
    }

    public function spcBaseQuery($query, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition) {

        // array_push($baseCondition, [
        //     'is_deleted', '=', 0
        // ]);

        // DB::listen(function ($query){
        //     Log::info($query->sql);
        //     Log::info($query->bindings);
        // });

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            // Log::info($baseCondition);

            foreach($baseCondition as $condition) {
                if($condition[1] === 'in') {
                    $query->whereIn($condition[0], $condition[2]);
                }
                else {
                    $query->where($condition[0], $condition[1], $condition[2]);
                }
            }
            
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        $result = $query->take($pageSize)->skip($startNum)->get();


        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query success',
            'data' => $result
        );
    }

    public function joinBaseQuery($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $relationship, $selectField, $subSelect=array()) {
        $query = DB::table($table);

        if (count($subSelect) > 0) {
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        if (  is_array($relationship) && count($relationship) > 0) {
            foreach($relationship as $relationData) {
                if ($relationData[1] == 'inner') {
                    $query->join($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                } else {
                    $query->leftJoin($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                }
            }
            
        }

        if ( is_array($selectField) && count($selectField) > 0) {
            foreach ($selectField as $key => $val) {
                $query->addSelect($val.'.'.$key);
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            $query->where($baseCondition);
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        try {
            $result = $query->take($pageSize)->skip($startNum)->get();
        }
        catch(\Exception $e) {
            return array(
                'success' => false,
                'totalCount' => 0,
                'message' => $e->getMessage(),
                'data' => null
            );
        }


        return array(
            'success' => true,
            'totalCount' => $totalCount,
            'message' => 'query '.$table.' success',
            'data' => $result
        );
    }

    public function joinBaseQueryForExcel($table, $pageNum, $pageSize, $baseCondition, $orCondition, $sortCondition, $relationship, $selectField) {
        $query = DB::table($table);
        $subSelect = array();
        if ( is_array($subSelect) && count($subSelect) > 0) {
            foreach ($subSelect as $key => $val) {
                $query->addSelect(DB::raw($val));
            }
        }

        if ( is_array($relationship) && count($relationship) > 0) {
            foreach($relationship as $relationData) {
                if ($relationData[1] == 'inner') {
                    $query->join($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                } else {
                    $query->leftJoin($relationData[0], $relationData[2], $relationData[3], $relationData[4]);
                }
            }
            
        }

        if ( is_array($selectField) && count($selectField) > 0) {
            foreach ($selectField as $key => $val) {
                if($key == 'product_code') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'product_name') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'order_nums') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'unit_price') {
                    $query->addSelect('order_detail.'.$key);
                }
                else if($key == 'detail_remark') {
                    $query->addSelect('order_detail.'.$key);
                }
                else {
                    $query->addSelect($val.'.'.$key);
                }
                
            }
        }

        $startNum = ($pageNum - 1) * $pageSize;

        if ((is_array($baseCondition)) && (count($baseCondition) > 0)) {
            $query->where($baseCondition);
        }

        if ( is_array($orCondition) && count($orCondition) > 0) {
            $query->where(function($q) use ($orCondition){
                $orLen = count($orCondition);
                for ($i = 0; $i < $orLen; $i++) {
                    if ($i === 0) {
                        $q->where($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                    else {
                        $q->orWhere($orCondition[$i][0], $orCondition[$i][1], $orCondition[$i][2]);
                    }
                }
                
            });
        }

        if ((is_array($sortCondition)) && (count($sortCondition) > 0)) {
            $query->orderBy($sortCondition[0]['sortField'], $sortCondition[0]['sortValue']);
        }

        $result = null;

        $totalCount = $query->count();

        return $query->take($pageSize)->skip($startNum)->get();
    }

    public function getAutoNumber($params){
        $model = new AutoNo();

        $numObj = $model->create([
            'num_format' => $params['num_format'],
            'length' => $params['length'],
        ]);
        return $numObj->auto_no;
    }
    public function getAutoNumberT($params){
        $model = new AutoNoT();

        $numObj = $model->create([
            'num_format' => $params['num_format'],
            'length' => $params['length'],
        ]);
        return $numObj->auto_no;
    }


    public function tolanguageStr($key) {
        $result = $key;
        $length = strlen($key);
        $thisWordCodeVerdeeld = array();
        for ($i=0; $i<$length; $i++) {
            if($key[$i-1] == "_" && $i >0 ) {
                $key[$i] =  strtoupper($key[$i]);
            } 
            $thisWordCodeVerdeeld[$i] = $key[$i];
        }
        
        $result = str_replace('_', '', implode('', $thisWordCodeVerdeeld));

        return $result;
    }
}
<?php

namespace App\Jobs;

use App\Inventory;
use App\InboundDetail;
use App\StockByProduct;
use App\StockByStorage;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class SyncInventory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $inventoryRepo;
    protected $productCode;
    protected $ownerId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productCode, $ownerId)
    {
        $this->productCode = $productCode;
        $this->ownerId     = $ownerId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // DB::beginTransaction();

        try {
            Log::info('start syncInventory');
            $product = Product::where('product_code', $this->productCode)
                        ->where('owner_id', $this->ownerId)
                        ->first();

            if($product) {
                Inventory::where('product_id', $product->product_id)
                ->update(array(
                    'product_name'     => $product->product_name,
                    'product_code'     => $product->product_code,
                    'product_barcode'  => $product->product_barcode,
                    'product_barcode2' => $product->product_barcode2,
                    'product_barcode3' => $product->product_barcode3,
                ));

                InboundDetail::where('product_id', $product->product_id)
                ->update(array(
                    'product_name'     => $product->product_name,
                    'product_code'     => $product->product_code,
                    'product_barcode'  => $product->product_barcode,
                    'product_barcode2' => $product->product_barcode2,
                    'product_barcode3' => $product->product_barcode3,
                ));

                StockByStorage::where('product_id', $product->product_id)
                ->update(array(
                    'product_name'     => $product->product_name,
                    'product_code'     => $product->product_code,
                    'product_barcode'  => $product->product_barcode,
                    'product_barcode2' => $product->product_barcode2,
                    'product_barcode3' => $product->product_barcode3,
                ));

                $stockByproducts = StockByProduct::where('product_id', $product->product_id)->where('available_nums', '>', 0)->get();
                Log::info($stockByproducts);
                foreach($stockByproducts as $stockByproduct) {
                    if($product->stock_minimal) {
                        StockByProduct::where('sbp_id', $stockByproduct->sbp_id)
                        ->update(array(
                            'stock_minimal' => $stockByproduct->available_nums - $product->stock_minimal
                        ));
                    }
                    else {
                        StockByProduct::where('sbp_id', $stockByproduct->sbp_id)
                        ->update(array(
                            'stock_minimal' => null
                        ));
                    }
                }

                StockByProduct::where('product_id', $product->product_id)
                ->update(array(
                    'product_name'     => $product->product_name,
                    'product_code'     => $product->product_code,
                    'product_barcode'  => $product->product_barcode,
                    'product_barcode2' => $product->product_barcode2,
                    'product_barcode3' => $product->product_barcode3,
                ));
            }
            
        }
        catch (Exception $e) {
            // DB::rollback();
            Log::error($e);
            return false;
        }

        // DB::commit();
        return true;
    }
}

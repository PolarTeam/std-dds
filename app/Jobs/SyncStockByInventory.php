<?php

namespace App\Jobs;

use App\Helpers\StockUpdate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class SyncStockByInventory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productIds)
    {
        $this->productIds = $productIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::info('start syncStockInventory');
            
            StockUpdate::updateStockByInventory($this->productIds);
        }
        catch (Exception $e) {
            Log::error($e);
            return false;
        }

        return true;
    }
}

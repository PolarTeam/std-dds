<?php

namespace App\Jobs;

use App\Jobs\Middleware\RateLimited;
use App\Helpers\OutsideApiHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class GraceGiftWebsiteApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $outboundStatus;
    protected $outboundIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($outboundIds, $outboundStatus)
    {
        $this->outboundIds    = $outboundIds;
        $this->outboundStatus = $outboundStatus;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            Log::info('GG API');
            $outsideApiHelper = new OutsideApiHelper();
            $outsideApiHelper->updateGraceGiftEcStatus($this->outboundIds, $this->outboundStatus);
        }
        catch (Exception $e) {
            Log::error($e);
            return false;
        }

        return true;
    }
}

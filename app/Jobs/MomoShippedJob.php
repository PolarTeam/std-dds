<?php

namespace App\Jobs;

use App\Services\RecOutboundService;
use App\Helpers\LogActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class MomoShippedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recOutboundService;
    protected $outboundIds;
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($outboundIds,RecOutboundService $recOutboundService)
    {
        $this->outboundIds        = $outboundIds;
        $this->recOutboundService = $recOutboundService;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            Log::info('MomoJob');
            $this->recOutboundService->momoToShippedForJob($this->outboundIds);
        }
        catch (Exception $e) {
            Log::error('MomoJobError');
            Log::error($e);
            return false;
        }

        return true;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds(5);
    }
}

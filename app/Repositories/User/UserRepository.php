<?php

namespace App\Repositories\User;

use App\User;
use App\Warehouse;
use App\WhUser;
use App\MenuItem;
use App\FunctionSchema;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as OClient;
use GuzzleHttp\Exception\ClientException;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\LogActivity;

class UserRepository implements UserRepositoryInterface {
    const SUCCUSUS_STATUS_CODE = 200;
    const UNAUTHORISED_STATUS_CODE = 401;
    const BASE_URL = 'https://std-wms.target-ai.com:17890';

    public function __construct(Client $client) {
        $this->http = $client;
    }

    public function register(Request $request) {
        $email = $request->email;
        $username = $request->username;
        $password = $request->password;
        $client = $request->client;
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        User::create($input);
        $response = $this->getTokenAndRefreshToken($username, $password, $client);
        return $this->response($response["data"], $response["statusCode"]);
    }

    public function login(Request $request) {
        $username = $request->username;
        $password = $request->password;
        $client   = $request->client;

        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();
            $Oclient = $this->getOClient($client);
            DB::table('oauth_access_tokens')
            ->where('user_id', $user->id)
            ->where('client_id', $Oclient->id)
            ->update([
                'revoked' => true
            ]);

            $response   = $this->getTokenAndRefreshToken($username, $password, $client);
            $data       = $response["data"];
            $statusCode = $response["statusCode"];

            $functionSchemas = FunctionSchema::where('company_id', $user->company_id)->orderBy('func_name', 'asc')->orderBy('seq', 'asc')->get();
            
            $funcSchema        = array();
            $oldFuncSchemaName = '';
            $oldRootId         = '';
            $schema            = array();
            foreach($functionSchemas as $idx => $functionSchema) {
                
                if($idx > 0 && $oldFuncSchemaName !== $functionSchema->func_name) {
                    $funcSchema[$oldFuncSchemaName] = [
                        'rootId' => $oldRootId,
                        'schema' => $schema
                    ];

                    $schema            = array();
                    $oldFuncSchemaName = $functionSchema->func_name;
                    $oldRootId         = $functionSchema->root_id;
                }
                
                array_push($schema, array(
                    'property'    => $functionSchema->field_name,
                    'displayName' => $functionSchema->display_name,
                    'type'        => $functionSchema->field_type
                ));
                $oldFuncSchemaName = $functionSchema->func_name;
                $oldRootId         = $functionSchema->root_id;

                if($idx === count($functionSchemas) - 1) {
                    $funcSchema[$functionSchema->func_name] = [
                        'rootId' => $functionSchema->root_id,
                        'schema' => $schema
                    ];
                }
            }

            $data['warehouse'] = Warehouse::where('warehouse.wh_id', $user->wh_id)->first();
            $data['warehouse1'] = Warehouse::join('wh_user', 'wh_user.wh_id', '=', 'warehouse.wh_id')
                                    ->select('warehouse.*')
                                    ->where('wh_user.user_id', $user->id)->get();
            $user = Auth::user();
            $menuItem = new MenuItem;
            $userAllPermissions = $user->getAllPermissions();
            $data['userData'] = $user;
            $data['data']  = array(
                'stageType'    => 'W',
                'userInfo'     => [
                    'avatar'              => 'https://i.loli.net/2017/08/21/599a521472424.jpg',
                    'userName'            => $user->name,
                    'nickname'            => $user->username,
                    'emial'               => $user->email,
                    'uid'                 => $user->id,
                    'userId'              => $user->id,
                    'uiId'                => 1,
                    'layoutMode'          => 'V',
                    'themeColor'          => $user->theme_color,
                    'phone'               => '1234567890',
                    'companyLocalName'    => null,
                    'companyEnglishName'  => null,
                    'companyKey'          => $user->company_id,
                    'warehousLocalName'   => $data['warehouse']->wh_name,
                    'warehousEnglishName' => null,
                    'warehouseKey'        => $user->wh_id,
                    'warehouseList'       => $data['warehouse1'],
                    'useFixedOwnerId'     => $data['warehouse']->use_fixed_owner_id
                ],
                'menuTree'       => $menuItem->getMenuItem($userAllPermissions),
                'permissions'    => $menuItem->getPermissions($userAllPermissions),
                'functionSchema' => $funcSchema
            );
            $data['success']   = true;
            LogActivity::addToLog('登入成功');
        } else {
            $data = [
                'success' => false,
                'message' => '帳號或密碼錯誤'
            ];
            $statusCode =  self::SUCCUSUS_STATUS_CODE;
        }

        return $this->response($data, $statusCode);
    }

    public function refreshToken(Request $request) {
        if (is_null($request->header('Refreshtoken'))) {
            return $this->response(['error'=>'Unauthorised'], self::UNAUTHORISED_STATUS_CODE);
        }

        $refresh_token = $request->header('Refreshtoken');
        $Oclient = $this->getOClient($request->client);
        $formParams = [ 'grant_type' => 'refresh_token',
                        'refresh_token' => $refresh_token,
                        'client_id' => $Oclient->id,
                        'client_secret' => $Oclient->secret,
                        'scope' => '*'];

        return $this->sendRequest("/oauth/token", $formParams);
    }

    public function details() {
        $user = Auth::user();
        return $this->response($user, self::SUCCUSUS_STATUS_CODE);
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return $this->response(['message' => 'Successfully logged out'], self::SUCCUSUS_STATUS_CODE);
    }

    public function response($data, int $statusCode) {
        $response = ["data"=>$data, "statusCode"=>$statusCode];
        return $response;
    }

    public function getTokenAndRefreshToken(string $username, string $password, string $client) {
        $Oclient = $this->getOClient($client);
        $formParams = [ 'grant_type' => 'password',
                        'client_id' => $Oclient->id,
                        'client_secret' => $Oclient->secret,
                        'username' => $username,
                        'password' => $password,
                        'scope' => '*'];

        return $this->sendRequest("/oauth/token", $formParams);
    }

    public function sendRequest(string $route, array $formParams) {
        try {
            $url = env('APP_URL').$route;
            $response = $this->curlRequest($url, $formParams);//$this->http->request('POST', $url, ['form_params' => $formParams]);
            $statusCode = self::SUCCUSUS_STATUS_CODE;
            $data = json_decode($response, true);//json_decode((string) $response->getBody(), true);
        } catch (ClientException $e) {
            $statusCode = $e->getCode();
            $data = ['error'=>'OAuth client error'];
        }

        return ["data" => $data, "statusCode"=>$statusCode];
    }

    public function getOClient($client) {
        return OClient::where('name', $client)->first();
    }

    public function getOnlineUser(Request $request) {
        $Oclient = $this->getOClient($request->client);
        $currentUsers = DB::table('oauth_access_tokens')->join('users', 'users.id', '=', 'oauth_access_tokens.user_id')
                        ->select('oauth_access_tokens.*')
                        ->where('users.is_admin', '<>', 1)
                        ->where('oauth_access_tokens.revoked', 0)
                        ->where('oauth_access_tokens.client_id', $Oclient->id)
                        ->get();

        $currentCnt = count($currentUsers);

        $userIds = array();

        foreach($currentUsers as $currentUser) {
            array_push($userIds, $currentUser->user_id);
        }

        $onlineUser = User::select('id', 'email', 'name', 'username')->whereIn('id', $userIds)->where('is_admin', '<>', 1)->get();
        
        $statusCode = self::SUCCUSUS_STATUS_CODE;
        
        return $this->response([
            'onlineUserCount' => $currentCnt,
            'user'            => $onlineUser,
            'superUser'       => ['timfine', 'tonyfine'],
            'onlineLimitUser' => (int)Crypt::decrypt(env('ONLINE_USER'))
        ], $statusCode);
    }

    public function superLogout(Request $request) {
        $Oclient = $this->getOClient($request->client);
        DB::table('oauth_access_tokens')
            ->where('user_id', $request->userId)
            ->where('client_id', $Oclient->id)
            ->update([
                'revoked' => true
            ]);


        return $this->response(['message' => 'Successfully logged out'], self::SUCCUSUS_STATUS_CODE);
    }

    private function curlRequest($url, $formParams) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => $formParams,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
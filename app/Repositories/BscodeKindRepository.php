<?php

namespace App\Repositories;

use App\BscodeKind;
use App\Bscode;
class BscodeKindRepository
{
    public function get($id) {
        return BscodeKind::find($id);
    }
    public function getBscodeByType($cdType) {
        return Bscode::where('cd_type', $cdType)->get();
    }
    public function create(array $data) {
        return BscodeKind::create($data);
    }

    public function batchInsert(array $data) {
        return BscodeKind::insert($data);
    }

    public function update($id, array $data) {
        $bscodeKind = BscodeKind::find($id);
        return $bscodeKind->update($data);
    }

    public function delete ($id) {
        $bscodeKind = BscodeKind::find($id);
        return $bscodeKind->delete();
    }
    
    public function getByCode ($cd) {
        return BscodeKind::where('cd', $cd)->first();
    }

    public function getbscode ($cdType) {
        return Bscode::where('cd_type', $cdType)->get();
    }
    
    public function createdetail(array $data) {
        return Bscode::create($data);
    }

    public function getDetailByCode ($cdType ,$cd) {
        return Bscode::where('cd_type', $cdType)->where('cd', $cd)->first();
    }
    public function updatedetail($id, array $data) {
        $bscode = Bscode::find($id);
        return $bscode->update($data);
    }

    public function deletedetail ($id) {
        $bscode = Bscode::find($id);
        return $bscode->delete();
    }


}
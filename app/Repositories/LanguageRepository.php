<?php

namespace App\Repositories;

use App\Muiltilang;
use App\MuiltilangDetail;
use Illuminate\Support\Facades\DB;
class LanguageRepository
{
    public function get($id) {
        return Muiltilang::find($id);
    }

    public function create(array $data) {
        return Muiltilang::create($data);
    }

    public function batchInsert(array $data) {
        return Muiltilang::insert($data);
    }

    public function update($id, array $data) {
        $muiltilang = Muiltilang::find($id);
        return $muiltilang->update($data);
    }

    public function updatedetail($id, array $data) {
        $muiltilangDetail = MuiltilangDetail::find($id);
        return $muiltilangDetail->update($data);
    }


    public function delete ($id) {
        $muiltilang = Muiltilang::find($id);
        return $muiltilang->delete();
    }

    public function getdetail($funcName) {
        $sql = "SELECT id, func_name, field_name, en_name, tw_name FROM function_schema_lang_detail WHERE func_name='".$funcName."'";
        $column = DB::select($sql);
        return $column;
    }

}
<?php

namespace App\Repositories;

use App\Sample;
use App\RecOutbound;

class SampleRepository
{
    public function get($id) {
        return Sample::find($id);
    }

    public function create(array $data) {
        return Sample::create($data);
    }

    public function batchInsert(array $data) {
        return Sample::insert($data);
    }

    public function update($id, array $data) {
        $sample = Sample::find($id);
        return $sample->update($data);
    }

    public function delete ($id) {
        $sample = Sample::find($id);
        return $sample->delete();
    }
}
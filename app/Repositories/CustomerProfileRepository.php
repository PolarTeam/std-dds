<?php

namespace App\Repositories;

use App\CustomerProfileModel;

class CustomerProfileRepository
{
    public function get($id) {
        return CustomerProfileModel::find($id);
    }

    public function create(array $data) {
        return CustomerProfileModel::create($data);
    }

    public function batchInsert(array $data) {
        return CustomerProfileModel::insert($data);
    }

    public function update($id, array $data) {
        $customerProfile = CustomerProfileModel::find($id);
        return $customerProfile->update($data);
    }

    public function delete ($id) {
        $customerProfile = CustomerProfileModel::find($id);
        return $customerProfile->delete();
    }
}
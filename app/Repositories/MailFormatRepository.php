<?php

namespace App\Repositories;

use App\MailFormat;
use Illuminate\Support\Facades\DB;

class MailFormatRepository
{
    public function get($id) {
        return MailFormat::find($id);
    }

    public function create(array $data) {
        return MailFormat::create($data);
    }

    public function batchInsert(array $data) {
        return MailFormat::insert($data);
    }

    public function update($id, array $data) {
        $mailFormat= MailFormat::find($id);
        return $mailFormat->update($data);
    }

    public function delete ($id) {
        $mailFormat= MailFormat::find($id);
        return $mailFormat->delete();
    }

    // key值判斷
    public function getByType ($type) {
        return MailFormat::where('type', $type)->first();
    }

}
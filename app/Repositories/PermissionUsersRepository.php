<?php

namespace App\Repositories;

use App\PermissionUsers;

class PermissionUsersRepository
{
    public function get($id) {
        return PermissionUsers::find($id);
    }

    public function getByuser($uesrId) {
        return PermissionUsers::where('user_id', $uesrId)->get();
    }

    public function create(array $data) {
        return PermissionUsers::create($data);
    }

    public function batchInsert(array $data) {
        return PermissionUsers::insert($data);
    }

    public function update($id, array $data) {
        $permissionUsers = PermissionUsers::find($id);
        return $permissionUsers->update($data);
    }

    public function delete ($id) {
        $permissionUsers = PermissionUsers::find($id);
        return $permissionUsers->delete();
    }

}
<?php

namespace App\Repositories;

use App\Permission;
use Illuminate\Support\Facades\DB;
class PermissionRepository
{
    public function get($id) {
        return Permission::find($id);
    }

    public function create(array $data) {
        return Permission::create($data);
    }

    public function batchInsert(array $data) {
        return Permission::insert($data);
    }

    public function update($id, array $data) {
        $permission = Permission::find($id);
        return $permission->update($data);
    }

    public function delete ($id) {
        $permission = Permission::find($id);
        return $permission->delete();
    }

    public function getByName ($name) {
        return Permission::where('name', $name)->first();
    }


    public function getwebPermission() {
        // $sql = "SELECT * FROM permissions WHERE type = 'web'";

        return DB::connection('mysql::write')
        ->table('permissions')
        ->where('type', 'web')
        ->orderBy('sorted','ASC')
        ->get();
    }

    public function getappPermission() {

        return DB::connection('mysql::write')
        ->table('permissions')
        ->where('type', 'app')
        ->orderBy('sorted','ASC')
        ->get();
    }

    public function getRolePermission($id) {

        return DB::connection('mysql::write')
        ->table('role_has_permissions')
        ->where('role_id', $id)
        ->pluck('permission_id')->toArray();
    }
    public function getPermissionNameByid($id) {
        return Permission::whereIn('id', $id)->orderBy('sorted','ASC')->pluck('name')->toArray();
    }
    
}
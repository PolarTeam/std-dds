<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;
use Illuminate\Http\Request;
use Backpack\LangFileManager\app\Models\Language;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class BaseModel extends Model
{
    
    public function getSearchData($request,$table_name,$status=true,$columns=null,$filter=null,$input=null) {
        try {
            $sql = " SET @@local.sort_buffer_size= 1024 * 1024 * 100; ";
            $exec = DB::select($sql);

            //預設空資料
            if($table_name == "emptygrid") {
                $data[] = array(
                    'TotalRows'   => null,
                    'Rows'        => null,
                    'StatusCount' => null
                );
                return $data;
            }

            $user = Auth::user();
            $ogtablename = $table_name;
            $table_name = $table_name;
            $this_query = null;
            if(isset($user)) {
                $this_query = DB::table($table_name);
            }
            else{
                $this_query = DB::table($table_name);
            }

            //20220509各table特殊條件
            // if($table_name == "mod_application_form_bykeeper") {
            //     $table_name = "mod_application_form";
            //     $this_query = DB::table($table_name);
            //     $this_query->whereRaw("(manage_by = '". $user->id."' or second_manage in('".$user->id."') ) ");
            // }
            if($ogtablename == "rec_inbound_detail_total") {
                $table_name = "rec_inbound_detail";
                $this_query = DB::table($table_name);
            }
            if($ogtablename == "mod_inventory_byuser") {
                $table_name = "mod_inventory";
                $this_query = DB::table($table_name);
                $this_query->Where(function($query) use ($user)
                {
                    $query->where("check_by", $user->name);
                    $query->orwhere("owner_by", $user->name);
                });
            }
            
            if($table_name == "mod_application_form_keeper") {
                $status=true;
                $table_name = "mod_application_form";
                $this_query = DB::table($table_name);
                $this_query->where('type', '!=', 'materiatransfer');
            }
            if($table_name == "mod_application_form_approver") {
                $status=true;
                $table_name = "mod_application_form";
                $this_query = DB::table($table_name);
                $this_query->where('status', '!=', 'FINISHED');
                $this_query->where('status', '!=', 'FAIL');
                $this_query->where('status', '!=', 'BACK');
                $this_query->Where(function($query) use ($user)
                {
                    $query->where("manage_by", $user->id);
                    $query->orwhere("second_manage","like","%".$user->id."%");
                });
            }
            if($table_name == "mod_application_form_approverclose") {
                $status=true;
                $table_name = "mod_application_form";
                $this_query = DB::table($table_name);
                $this_query->whereIn('status', ['FINISHED','FAIL','BACK']);
                $this_query->Where(function($query) use ($user)
                {
                    $query->where("manage_by", $user->id);
                    $query->orwhere("second_manage","like","%".$user->id."%");
                });
            }
            if($table_name == "mod_application_form_bymy") {
                $status=true;
                $table_name = "mod_application_form";
                $this_query = DB::table($table_name);
                $this_query->whereRaw("(created_by = '". $user->id."' or updated_by = '".$user->id."' ) ");
            }
            if($table_name == "mod_application_form") {
                $status=true;
            }
            //20220509各table特殊條件
            if($table_name == "mod_application_list") {
                $this_query->where('created_by', $user->id);
            }
 
            //20220509各table特殊條件
            if($table_name == "mod_application_form") {
                $this_query->where('hidden_flag','N');
            }

            //20220509各table特殊條件
            if($table_name == "mod_putaway") {
                $this_query->where('status', '!=', 'Y');
            }
            
            // 各table特殊條件 end
            $startnum = $request->pagenum * $request->pagesize;
            if($columns != null){
                $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
            }else{
                $frist_col = "";
            }

            if(isset($request->basecon)) {
                $filters = explode(')*(', $request->basecon);
                if(count($filters) > 0) {
                    for($i=0; $i<count($filters); $i++) {
                        $con_array = explode(";", $filters[$i]);
                        $df = $con_array[0];
                        $cf = $con_array[1];
                        $vf = $con_array[2];

                        switch($cf) {
                            case "EQUAL":
                                $cf = "=";
                                break;
                            case "NOT_EQUAL":
                                $cf = "<>";
                                break;
                            case "IN":
                                $cf = "in";

                                break;
                            default:
                                $cf = "=";
                                break;
                        }
                        if( $cf == "in") {
                            $this_query->whereIn($df, json_decode($vf));
                        }else {
                            $this_query->where($df, $cf, $vf);
                        }
                        
                    }
                }
            }

            if(isset($request->dbRaw)) {
                $this_query->whereRaw(Crypt::decrypt($request->dbRaw));
            }

            if(isset($request->defOrder)) {
                $order_array = explode(";", $request->defOrder);
                $df = $order_array[0];
                $cf = $order_array[1];

                $this_query->orderBy($df, $cf);
            }

            if (isset($request->filterscount)) {
                $filterscount = $request->filterscount;
                if ($filterscount > 0) {
                    $where_type = true; // true means AND, false means OR
                    $tmpdatafield = "";
                    $tmpfilteroperator = "";
                    $valuesPrep = "";
                    $values = [];
                    $date_format = "";
                    for ($i = 0; $i < $filterscount; $i++) {
                        // get the filter's value.
                        $filtervalue = $request["filtervalue" . $i];
                        // get the filter's condition.
                        $filtercondition = $request["filtercondition" . $i];
                        // get the filter's column.
                        $filterdatafield = $request["filterdatafield" . $i];
                        // get the filter's operator.
                        $filteroperator = $request["filteroperator" . $i];
                        if ($tmpdatafield == "")
                        {
                        $tmpdatafield = $filterdatafield;
                        }
                        else if ($tmpdatafield <> $filterdatafield)
                        {
                            $where_type = true;
                        }
                        else if ($tmpdatafield == $filterdatafield)
                        {
                        if ($tmpfilteroperator == 0)
                        {
                            $where_type = true;
                        }
                        else $where_type = false;
                        }
                        // build the "WHERE" clause depending on the filter's condition, value and datafield.
                        $multiValue = explode(';', $filtervalue);
                        switch ($filtercondition)
                        {
                        case "CONTAINS":
                            $condition = " LIKE ";
                            if( is_array($multiValue) && count($multiValue ) > 1) {
                                for($k=0;$k<count($multiValue);$k++) {
                                    if($k == 0) {
                                        $value = "%{$multiValue[$k]}%;";
                                    }
                                    else {
                                        $value .= "%{$multiValue[$k]}%;";
                                    }
                                    
                                }
                                $value = rtrim($value,";");
                            }
                            else {
                                $value = "%{$multiValue[0]}%";
                            }
                            
                            break;
                        case "CONTAINSLIKE":
                            $condition = "like_in";
                            $value = $filtervalue;
                            break;
                        case "DOES_NOT_CONTAIN":
                            $condition = " NOT LIKE ";
                            $value = "%{$filtervalue}%";
                            break;
                        case "EQUAL":
                            $condition = " = ";
                            $value = $filtervalue;
                            break;
                        case "NOT_EQUAL":
                            $condition = "not_in";
                            $value = $filtervalue;
                            break;
                        case "IN":
                            $condition = "in";
                            $value = $filtervalue;
                            break;
                        case "GREATER_THAN":
                            $condition = " > ";
                            $value = $filtervalue;
                            break;
                        case "LESS_THAN":
                            $condition = " < ";
                            $value = $filtervalue;
                            break;
                        case "GREATER_THAN_OR_EQUAL":
                            $condition = " >= ";
                            $value = $filtervalue;
                            break;
                        case "LESS_THAN_OR_EQUAL":
                            $condition = " <= ";
                            $value = $filtervalue;
                            break;
                        case "STARTS_WITH":
                            $condition = " LIKE ";
                            $value = "{$filtervalue}%";
                            break;
                        case "ENDS_WITH":
                            $condition = " LIKE ";
                            $value = "%{$filtervalue}";
                            break;
                        case "NULL":
                            $condition = " IS NULL ";
                            $value = "%{$filtervalue}%";
                            break;
                        case "NOT_NULL":
                            $condition = " IS NOT NULL ";
                            $value = "%{$filtervalue}%";
                            break;
                        }

                        if($this->validateDateTime($value)){
                            switch($filterdatafield){
                                case "created_at":
                                case "updated_at":
                                    //$value =  strtotime($value);
                                    $date_format = "timestamp";
                                default:
                                    $date_format = "datetime";
                                break;
                            }
                        }elseif($this->validateDateTime($value)){
                            $date_format = "date";
                        }
        
                        try{
                            if($where_type){
                                if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                    // $d = Carbon::parse($value)->format('Y-m-d');
                                    if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                        $this_query->whereDate($filterdatafield,trim($condition), $value);
                                    }else{
                                        $this_query->where($filterdatafield,trim($condition), $value);
                                        // dd($filterdatafield."/".trim($condition)."/".$value);
                                    }
                                }else{
                                    $multiValue = explode(';', $value);

                                    if(count($multiValue) > 1) {
                                        $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                            for($k=0;$k<count($multiValue); $k++) {
                                                $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                            }
                                        });
                                    }
                                    else {
                                        //$this_query->where($filterdatafield,  trim($condition), $value);
                                        if( trim($condition) == "not_in") {
                                            $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                        } else if(trim($condition) == "like_in") {
                                            $this_query->Where(function($query) use ($filterdatafield, $value)
                                            {
                                                $multiValues = explode(',', $value);
                                                foreach ($multiValues as $key => $multiValue) {
                                                    $query->orwhere($filterdatafield,"like","%".$multiValue."%");
                                                }
                                            });
                                        } else if(trim($condition) == "in") {
                                            $this_query->whereIn($filterdatafield, explode(',', $multiValue[0]));
                                        }  else {
                                            $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                        }
                                    }
                                    
                                }
                                
                            }else{
                                $this_query->orWhere($filterdatafield, trim($condition), $value);
                            }
                                $tmpfilteroperator = $filteroperator;
                                $tmpdatafield = $filterdatafield;
                            
                        }catch(\Exception $ex){
                            return $ex;
                        }
                    }
                }
            }
            if($filter!=null){
                $this_query->whereRaw($filter);
            }

            if($input!=null && $columns!=null){
                $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
            }
            // //特別處理groupby
            // if($ogtablename == "rec_inbound_detail_total") {
            //     $this_query->groupBy('part_no');
            //     $this_query->groupBy('project');
            //     $this_query->groupBy('part_name');
            //     $this_query->groupBy('barcode');
            //     $this_query->groupBy('use_project');
            //     // rec_inbound_detail_total
            // }
            $count_query = clone $this_query;

            if($status){
                $status_count = $count_query->select(DB::raw("count(*) as count, status, '' AS statustext"))
                ->groupBy('status')
                ->orderBy('status', 'ASC' )
                ->get();
                foreach ($status_count as $sc){
                    $sc->statustext = trans($this->dashesToCamelCase($table_name).'.STATUS'.$sc->status);
                }
            }else{
                $status_count = 0;
            }

            if(isset($request->sortdatafield) && isset($request->sortorder)){
                $sortfield = $request->sortdatafield;
                $sortorder = $request->sortorder;
                $this_query->orderBy($sortfield,$sortorder);
            }
        
            if($input==null){
                $total_rows = $this_query->count();
            }
        
            if($columns==null){
                $rows = $this_query->take($request->pagesize)->skip($startnum)->get();
            }else{
                if($input!=null){
                    $rows = $this_query->select(DB::raw($frist_col." AS label,".$frist_col." AS value".",".$columns))->take(10)->get();
                    $data = $rows;
                    return $data;
                    
                }else{
                    $rows = $this_query->select(DB::raw($columns))->take($request->pagesize)->skip($startnum)->get();//
                }
            
            }
            
            // Log::info('FOR INDEX');
            // $bindings = $this_query->getBindings();
            // $sql = str_replace('?', '%s', $this_query->toSql());
            // $sql = sprintf($sql, ...$bindings);
            // Log::info($sql);
            // Log::info('FOR INDEX END');
        
            $data[] = array(
                'TotalRows' => $total_rows,
                'Rows' => $rows,
                'StatusCount' => $status_count
            );
            return $data;
        } catch (\Throwable $e) {
            Log::info($e->getMessage());
            Log::info($e->getLine());
            $data[] = array(
                'TotalRows'   => null,
                'Rows'        => null,
                'StatusCount' => null
            );
            return $data;
            //throw $th;
        }
    }

    public function getinboundbymy($request,$table_name,$status=true,$columns=null,$filter=null,$input=null) {
        $user = Auth::user();

        $total_rows = null;
        $rows = null;
        $ids = array();
        if($filter != null ) {
            $user->id = $filter;
        }

        $consumables = DB::table('inbound_detail_owner')
        ->select('inbound_id')
        ->where('created_by', $user->id)
        ->where('status','!=','FAIL')
        ->where('num', '>', 0)
        ->get();

        $normals = DB::table('rec_inbound_detail')
        ->select('inbound_id')
        ->where('consumables','N')
        ->where('owner_by', $user->id)
        ->get();

        foreach ($consumables as $key => $consumable) {
            $ids[] = $consumable->inbound_id;
        }
        foreach ($normals as $key => $normal) {
            $ids[] = $normal->inbound_id;
        }

        $sqlstr = "";
        $this_query = DB::table('rec_inbound')
        ->select('*', DB::raw( '0 as jpn_num') )
        ->whereIn('id', $ids)
        ->groupBy('part_no', 'part_name', 'project', 'mergejpn');
        //
        if (isset($request->filterscount)) {
            $filterscount = $request->filterscount;
            if ($filterscount > 0) {
                $where_type = true; // true means AND, false means OR
                $tmpdatafield = "";
                $tmpfilteroperator = "";
                $valuesPrep = "";
                $values = [];
                $date_format = "";
                for ($i = 0; $i < $filterscount; $i++) {
                    // get the filter's value.
                    $filtervalue = $request["filtervalue" . $i];
                    // get the filter's condition.
                    $filtercondition = $request["filtercondition" . $i];
                    // get the filter's column.
                    $filterdatafield = $request["filterdatafield" . $i];
                    // get the filter's operator.
                    $filteroperator = $request["filteroperator" . $i];
                    if ($tmpdatafield == "")
                    {
                    $tmpdatafield = $filterdatafield;
                    }
                    else if ($tmpdatafield <> $filterdatafield)
                    {
                        $where_type = true;
                    }
                    else if ($tmpdatafield == $filterdatafield)
                    {
                    if ($tmpfilteroperator == 0)
                    {
                        $where_type = true;
                    }
                    else $where_type = false;
                    }
                    // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    switch ($filtercondition)
                    {
                    case "CONTAINS":
                        $condition = " LIKE ";
                        if( is_array($multiValue) && count($multiValue ) > 1) {
                            for($k=0;$k<count($multiValue);$k++) {
                                if($k == 0) {
                                    $value = "%{$multiValue[$k]}%;";
                                }
                                else {
                                    $value .= "%{$multiValue[$k]}%;";
                                }
                                
                            }
                            $value = rtrim($value,";");
                        }
                        else {
                            $value = "%{$multiValue[0]}%";
                        }
                        
                        break;
                    case "CONTAINSLIKE":
                        $condition = "like_in";
                        $value = $filtervalue;
                        break;
                    case "DOES_NOT_CONTAIN":
                        $condition = " NOT LIKE ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "EQUAL":
                        $condition = " = ";
                        $value = $filtervalue;
                        break;
                    case "NOT_EQUAL":
                        $condition = "not_in";
                        $value = $filtervalue;
                        break;
                    case "IN":
                        $condition = "in";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN":
                        $condition = " > ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN":
                        $condition = " < ";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $condition = " >= ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $condition = " <= ";
                        $value = $filtervalue;
                        break;
                    case "STARTS_WITH":
                        $condition = " LIKE ";
                        $value = "{$filtervalue}%";
                        break;
                    case "ENDS_WITH":
                        $condition = " LIKE ";
                        $value = "%{$filtervalue}";
                        break;
                    case "NULL":
                        $condition = " IS NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "NOT_NULL":
                        $condition = " IS NOT NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    }

                    if($this->validateDateTime($value)){
                        switch($filterdatafield){
                            case "created_at":
                            case "updated_at":
                                //$value =  strtotime($value);
                                $date_format = "timestamp";
                            default:
                                $date_format = "datetime";
                            break;
                        }
                    }elseif($this->validateDateTime($value)){
                        $date_format = "date";
                    }
    
                    try{
                        if($where_type){
                            if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                // $d = Carbon::parse($value)->format('Y-m-d');
                                if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                    $this_query->whereDate($filterdatafield,trim($condition), $value);
                                }else{
                                    $this_query->where($filterdatafield,trim($condition), $value);
                                    // dd($filterdatafield."/".trim($condition)."/".$value);
                                }
                            }else{
                                $multiValue = explode(';', $value);

                                if(count($multiValue) > 1) {
                                    $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                        for($k=0;$k<count($multiValue); $k++) {
                                            $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                        }
                                    });
                                }
                                else {
                                    //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else if(trim($condition) == "like_in") {
                                        $this_query->Where(function($query) use ($filterdatafield, $value)
                                        {
                                            $multiValues = explode(',', $value);
                                            foreach ($multiValues as $key => $multiValue) {
                                                $query->orwhere($filterdatafield,"like","%".$multiValue."%");
                                            }
                                        });
                                    } else if(trim($condition) == "in") {
                                        $this_query->whereIn($filterdatafield, explode(',', $multiValue[0]));
                                    }  else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                                }
                                
                            }
                            
                        }else{
                            $this_query->orWhere($filterdatafield, trim($condition), $value);
                        }
                            $tmpfilteroperator = $filteroperator;
                            $tmpdatafield = $filterdatafield;
                        
                    }catch(\Exception $ex){
                        return $ex;
                    }
                }
            }
        }

        if($filter!=null){
            $this_query->whereRaw($filter);
        }

        if($columns != null){
            $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
        }else{
            $frist_col = "";
        }
        if($input!=null && $columns!=null){
            $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
        }
        if(isset($request->sortdatafield) && isset($request->sortorder)){
            $sortfield = $request->sortdatafield;
            $sortorder = $request->sortorder;
            $this_query->orderBy($sortfield,$sortorder);
        }
        //
        $resultdata =$this_query->get();

        foreach ($resultdata as $key => $row) {
            if($row->consumables == "Y") {
                $row->jpn_num = DB::table('inbound_detail_owner')
                ->select(DB::raw('sum(num) as total_num'))
                ->where('created_by', $user->id)
                ->where('status','!=','FAIL')
                ->where('num', '>', 0)
                ->where('part_no', $row->part_no)
                ->where('project',  $row->project)
                ->where('part_name',  $row->part_name)
                ->where('mergejpn',  $row->mergejpn)
                ->value('total_num');

            } else {
                $row->jpn_num = DB::table('rec_inbound_detail')
                ->select(DB::raw('sum(num) as total_num'))
                ->where('owner_by', $user->id)
                ->where('part_no', $row->part_no)
                ->where('project',  $row->project)
                ->where('mergejpn',  $row->mergejpn)
                ->where('part_name',  $row->part_name)
                ->value('total_num');
            }
            # code...
        }

        $data[] = array(
            'TotalRows'   => count($resultdata),
            'Rows'        => $resultdata,
            'StatusCount' => null
        );

        return $data;
    }
    public function getinboundbymyall($request,$table_name,$status=true,$columns=null,$filter=null,$input=null) {
        $user = Auth::user();
        
        $total_rows = null;
        $rows = null;
        $ids = array();
        if($filter != null ) {
            $user->id = $filter;
        }

        $consumables = DB::table('inbound_detail_owner')
        ->select('inbound_id')
        ->where('status','!=','FAIL')
        ->where('num', '>', 0)
        ->get();

        $normals = DB::table('rec_inbound_detail')
        ->select('inbound_id')
        ->whereNotNull('owner_by')
        ->get();

        foreach ($consumables as $key => $consumable) {
            $ids[] = $consumable->inbound_id;
        }
        foreach ($normals as $key => $normal) {
            $ids[] = $normal->inbound_id;
        }

        $sqlstr = "";
        $this_query = DB::table('rec_inbound_view_all')
        ->select('*')
        ->whereIn('id', $ids)
        ->groupBy('part_no', 'part_name', 'project');
        //
        if (isset($request->filterscount)) {
            $filterscount = $request->filterscount;
            if ($filterscount > 0) {
                $where_type = true; // true means AND, false means OR
                $tmpdatafield = "";
                $tmpfilteroperator = "";
                $valuesPrep = "";
                $values = [];
                $date_format = "";
                for ($i = 0; $i < $filterscount; $i++) {
                    // get the filter's value.
                    $filtervalue = $request["filtervalue" . $i];
                    // get the filter's condition.
                    $filtercondition = $request["filtercondition" . $i];
                    // get the filter's column.
                    $filterdatafield = $request["filterdatafield" . $i];
                    // get the filter's operator.
                    $filteroperator = $request["filteroperator" . $i];
                    if ($tmpdatafield == "")
                    {
                    $tmpdatafield = $filterdatafield;
                    }
                    else if ($tmpdatafield <> $filterdatafield)
                    {
                        $where_type = true;
                    }
                    else if ($tmpdatafield == $filterdatafield)
                    {
                    if ($tmpfilteroperator == 0)
                    {
                        $where_type = true;
                    }
                    else $where_type = false;
                    }
                    // build the "WHERE" clause depending on the filter's condition, value and datafield.
                    $multiValue = explode(';', $filtervalue);
                    switch ($filtercondition)
                    {
                    case "CONTAINS":
                        $condition = " LIKE ";
                        if( is_array($multiValue) && count($multiValue ) > 1) {
                            for($k=0;$k<count($multiValue);$k++) {
                                if($k == 0) {
                                    $value = "%{$multiValue[$k]}%;";
                                }
                                else {
                                    $value .= "%{$multiValue[$k]}%;";
                                }
                                
                            }
                            $value = rtrim($value,";");
                        }
                        else {
                            $value = "%{$multiValue[0]}%";
                        }
                        
                        break;
                    case "CONTAINSLIKE":
                        $condition = "like_in";
                        $value = $filtervalue;
                        break;
                    case "DOES_NOT_CONTAIN":
                        $condition = " NOT LIKE ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "EQUAL":
                        $condition = " = ";
                        $value = $filtervalue;
                        break;
                    case "NOT_EQUAL":
                        $condition = "not_in";
                        $value = $filtervalue;
                        break;
                    case "IN":
                        $condition = "in";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN":
                        $condition = " > ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN":
                        $condition = " < ";
                        $value = $filtervalue;
                        break;
                    case "GREATER_THAN_OR_EQUAL":
                        $condition = " >= ";
                        $value = $filtervalue;
                        break;
                    case "LESS_THAN_OR_EQUAL":
                        $condition = " <= ";
                        $value = $filtervalue;
                        break;
                    case "STARTS_WITH":
                        $condition = " LIKE ";
                        $value = "{$filtervalue}%";
                        break;
                    case "ENDS_WITH":
                        $condition = " LIKE ";
                        $value = "%{$filtervalue}";
                        break;
                    case "NULL":
                        $condition = " IS NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    case "NOT_NULL":
                        $condition = " IS NOT NULL ";
                        $value = "%{$filtervalue}%";
                        break;
                    }

                    if($this->validateDateTime($value)){
                        switch($filterdatafield){
                            case "created_at":
                            case "updated_at":
                                //$value =  strtotime($value);
                                $date_format = "timestamp";
                            default:
                                $date_format = "datetime";
                            break;
                        }
                    }elseif($this->validateDateTime($value)){
                        $date_format = "date";
                    }
    
                    try{
                        if($where_type){
                            if($date_format == "timestamp" || $date_format == "datetime" || $date_format == "date"){
                                // $d = Carbon::parse($value)->format('Y-m-d');
                                if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)){
                                    $this_query->whereDate($filterdatafield,trim($condition), $value);
                                }else{
                                    $this_query->where($filterdatafield,trim($condition), $value);
                                    // dd($filterdatafield."/".trim($condition)."/".$value);
                                }
                            }else{
                                $multiValue = explode(';', $value);

                                if(count($multiValue) > 1) {
                                    $this_query->where(function ($query) use ($multiValue, $filterdatafield, $condition) {
                                        for($k=0;$k<count($multiValue); $k++) {
                                            $query->orWhere($filterdatafield,  trim($condition), $multiValue[$k]);
                                        }
                                    });
                                }
                                else {
                                    //$this_query->where($filterdatafield,  trim($condition), $value);
                                    if( trim($condition) == "not_in") {
                                        $this_query->whereNotIn($filterdatafield, explode(',', $multiValue[0]));
                                    } else if(trim($condition) == "like_in") {
                                        $this_query->Where(function($query) use ($filterdatafield, $value)
                                        {
                                            $multiValues = explode(',', $value);
                                            foreach ($multiValues as $key => $multiValue) {
                                                $query->orwhere($filterdatafield,"like","%".$multiValue."%");
                                            }
                                        });
                                    } else if(trim($condition) == "in") {
                                        $this_query->whereIn($filterdatafield, explode(',', $multiValue[0]));
                                    }  else {
                                        $this_query->where($filterdatafield,  trim($condition), $multiValue[0]);
                                    }
                                }
                                
                            }
                            
                        }else{
                            $this_query->orWhere($filterdatafield, trim($condition), $value);
                        }
                            $tmpfilteroperator = $filteroperator;
                            $tmpdatafield = $filterdatafield;
                        
                    }catch(\Exception $ex){
                        return $ex;
                    }
                }
            }
        }

        if($filter!=null){
            $this_query->whereRaw($filter);
        }

        if($columns != null){
            $frist_col = "CONCAT(".str_replace("+", ",' : ',", explode(",", $columns)[0]).")";
        }else{
            $frist_col = "";
        }
        if($input!=null && $columns!=null){
            $this_query->whereRaw($frist_col." LIKE "."'%$input%'");
        }
        if(isset($request->sortdatafield) && isset($request->sortorder)){
            $sortfield = $request->sortdatafield;
            $sortorder = $request->sortorder;
            $this_query->orderBy($sortfield,$sortorder);
        }
        //
        $resultdata =$this_query->get();

        foreach ($resultdata as $key => $row) {
            if($row->consumables == "Y") {
                $row->jpn_num = DB::table('inbound_detail_owner')
                ->select(DB::raw('sum(num) as total_num'))
                ->where('status','!=','FAIL')
                ->where('num', '>', 0)
                ->where('part_no', $row->part_no)
                ->where('project',  $row->project)
                ->where('part_name',  $row->part_name)
                ->value('total_num');

            } else {
                $row->jpn_num = DB::table('rec_inbound_detail')
                ->select(DB::raw('sum(num) as total_num'))
                ->whereNotNull('owner_by')
                ->where('part_no', $row->part_no)
                ->where('project',  $row->project)
                ->where('part_name',  $row->part_name)
                ->value('total_num');
            }
            # code...
        }

        $data[] = array(
            'TotalRows'   => count($resultdata),
            'Rows'        => $resultdata,
            'StatusCount' => null
        );

        return $data;
    }
    public function validateDate($date, $format = 'Y-m-d')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return ex;
        }
       
    }

    public function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        try{
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }catch(\Exception $ex){
            return ex;
        }
       
    }

    public function getColumnInfo($table_name,$columns=null,$languagename=null,$lang=null){
        $user = Auth::user();
        $cols_sql = "";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }
        // text
        session(['locale' => $lang]);
        app()->setLocale($lang);

        $values_source = "";
        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $this_type = explode("(", $value->Type)[0];
            $filtertype = "input";
            $cellsformat = "";
            $width = filter_var($value->Type, FILTER_SANITIZE_NUMBER_INT);
            $width = $width==""?100:$width;
            $db_width = $width;
            $cellsalign = "left";
            $statusCode = array();
            // if($value->Field == 'status' &&$table_name!='mod_order_total_view' &&$table_name!='mod_order_detail_close_view') {
            //     $status = $this->get_enum_values($value->Type);
            //     for($i=0; $i<count($status); $i++) {
            //         $statusCode[$i] = array (
            //             'label' => trans($this->dashesToCamelCase($table_name).'.STATUS'.$status[$i]),
            //             'value' => $status[$i]
            //         );
            //     }
            // }

            $trsMode = array();
            if($value->Field == 'trs_mode') {
                $trsData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'TM')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                
                if(count($trsData) > 0) {
                    foreach($trsData as $key=>$row) {
                        $trsMode[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $carType = array();
            if($value->Field == 'car_type') {
                $baseData = DB::table('bscode')
                    ->select('cd', 'cd_descp')
                    ->where('cd_type', 'CARTYPE')
                    ->where('g_key', $user->g_key)
                    ->where('c_key', $user->c_key)                    
                    ->get();
                if(count($baseData) > 0) {
                    foreach($baseData as $key=>$row) {
                        $carType[$key] = array (
                            'label' => $row->cd_descp,
                            'value' => $row->cd
                        );
                    }
                    
                }
            }

            $dlvType = array();
            if($value->Field == 'dlv_type') {
                $dlvType[0] = array (
                    'label' => '提貨(P)',
                    'value' => 'P'
                );

                $dlvType[1] = array (
                    'label' => '配送(D)',
                    'value' => 'D'
                );
            }

            switch($this_type){
                case "timestamp":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "date":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "datetime":
                    $this_type = "range";
                    $filtertype = "range";
                    $cellsformat = 'yyyy-MM-dd HH:mm:ss';
                    $cellsalign = "left";
                    $width=100;
                    break;
                case "decimal":
                case "int":
                case "float":
                    $this_type = "number";
                    $filtertype = "number";
                    $cellsalign = "right";
                    $width*=3;
                    break;
                case "text":
                case "varchar":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                break;
                case "enum":
                    $this_type = "string";
                    $filtertype = "textbox";
                    $cellsalign = "left";
                    $width*=3;
                    $values_source = array('source' =>  $value->Field."GridObj",
                                        'value' => "val", 
                                        'name'  => "key");
                break;
                default:
                    $width*=3;
                break;
            }

            if($width < 100){
                $width = 100;
            }

           
            $fields_info[] = array(
                'name' => $value->Field,
                'type' => $this_type
            );
         
            // $columns_model[] = array(
            //     'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
            //     'datafield'   => $value->Field,
            //     'width'       => $width,
            //     'dbwidth'     => $db_width,
            //     'nullable'    => $value->Null == "YES"?true: false,
            //     'cellsformat' => $cellsformat,
            //     'filtertype'  => $filtertype,
            //     'cellsalign'  => $cellsalign,
            //     'values'      => $values_source
            // );
            if($value->Field == "status") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'statusCode'  => $statusCode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "trs_mode") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'trsMode'     => $trsMode,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "car_type") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'carType'     => $carType,
                    'filterdelay' => 99999999
                );
            }
            else if($value->Field == "dlv_type") {
                $columns_model[] = array(
                    'text'        => trans($this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'dlvType'     => $dlvType,
                    'filterdelay' => 99999999
                );
            }
            else {
                $columns_model[] = array(
                    'text'        => trans($languagename.'.'.$this->dashesToCamelCase($value->Field)),//$value->Field,
                    'datafield'   => $value->Field,
                    'width'       => $width,
                    'dbwidth'     => $db_width,
                    'nullable'    => $value->Null == "YES"?true: false,
                    'cellsformat' => $cellsformat,
                    'filtertype'  => $filtertype,
                    'cellsalign'  => $cellsalign,
                    'values'      => $values_source,
                    'filterdelay' => 99999999
                );
            }

            $values_source = "";
           // dd($value->Type);
           //echo "'" . $value->Field . "' => '" . $value->Type . "|" . ( $value->Null == "NO" ? 'required' : '' ) ."', <br/>" ;
        }

        /*
        
            { text: 'id', datafield: 'id' , width: 250 },
                { text: 'name', datafield: 'name' , width: 250 },
                { text: 'email', datafield: 'email' , width: 250 },
                { text: 'password', datafield: 'password' , width: 250 },
                { text: 'Group', datafield: 'g_key' , width: 250 },
                { text: 'Company', datafield: 'c_key' , width: 250 },
                { text: 'Station', datafield: 's_key' , width: 250 },
                { text: 'Derpartment', datafield: 'd_key' , width: 250 },
                { text: 'remember_token', datafield: 'remember_token' , width: 250 },
                { text: 'created_at',filtertype: 'date', datafield: 'created_at' , width: 250, cellsformat: 'yyyy-MM-dd' },
                { text: 'updated_at',filtertype: 'date', datafield: 'updated_at' , width: 250 },
        
        */
        $data[0] = $fields_info;
        $data[1] = $columns_model;

        return $data;
    }

    public function getColumnSpec($table_name,$columns=null){
        $cols_sql = "";
        $spec_text = "return [<br />";
        if($columns!=null){
            $cols_sql = " WHERE Field IN  ('".str_replace(",", "','", $columns)."')";
        }

        $columns = DB::select('show columns from ' . $table_name . $cols_sql);
        foreach ($columns as $value) {
            $spec_text.= '"'.$this->dashesToCamelCase($value->Field).'"        =>      "'.trans(''.$this->dashesToCamelCase($table_name).'.'.$this->dashesToCamelCase($value->Field)).'",<br />';
        }
        $spec_text.="];";

        return $spec_text;
    }

    public function saveLayout($key,$data,$lang,$user){

        try{
            $now = date("Y-m-d H:i:s");
            DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', $lang)->delete();

            $id = DB::table('sys_layout')->insertGetId([
                'key'        => $key,
                'data'       => $data,
                'lang'       => $lang,
                'g_key'      => $user->g_key,
                'c_key'      => $user->c_key,
                'd_key'      => $user->d_key,
                's_key'      => $user->s_key,
                'created_at' => $now,
                'updated_at' => $now,
                'created_by' => $user->email,
                'updated_by' => $user->email
            ]);
            return "true";
        }catch(\Exception $ex){
            return "false";
        }
    }

    public function getLayout($key,$lang){
        $user = Auth::user();
        try{
            $layout = array();
            $checklayout = DB::table('sys_layout')
            ->where('key', '=', $key)
            ->where('created_by', $user->email)
            ->where('lang', '=', $lang)
            ->first();
            if(isset($checklayout)) {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', $user->email)
                ->where('lang', '=', $lang)
                ->pluck('data');
            } else {
                $layout = DB::table('sys_layout')
                ->where('key', '=', $key)
                ->where('created_by', 'standardlayout@standard-info.com')
                ->where('lang', '=', $lang)
                ->pluck('data');
            }


            return $layout[0];
        }catch(\Exception $ex){
            return "false";
        }
        
    }

    function dashesToCamelCase($string, $capitalizeFirstCharacter = false) 
    {
    
        $str = str_replace('_', '', ucwords($string, '_'));
    
        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
    
        return $str;
    }


    public static function getPossbileStatuses($tableName, $colName){
        $type = DB::select(DB::raw('SHOW COLUMNS FROM '.$tableName.' WHERE Field = "'.$colName.'"'))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $values = array();
        foreach(explode(',', $matches[1]) as $value){
            $values[] = trim($value, "'");
        }
        return $values;
    }


    function get_enum_values( $type ) {
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    function getStatusCount($tableName, $langname=null) {
        $user = Auth::user();
        $status_count = DB::table($tableName)->select(DB::raw("count(*) as count, status, '' AS statustext"))
        ->groupBy('status')
        ->orderBy('status', 'ASC' )
        ->get();
        foreach ($status_count as $sc){
            $sc->statustext = trans($this->dashesToCamelCase($tableName).'.STATUS'.$sc->status);
        }
        $this_query = DB::table($tableName);
        if($tableName == "mod_application_form_keeper") {
            $tableName = "mod_application_form";
            $this_query = DB::table($tableName);
            $this_query->whereIn('status', ["SUCCESS","PREPARATION"]);
            $this_query->where('status', '!=', 'FAIL');
            $this_query->where('status', '!=', 'BACK');
            $this_query->where('type', '!=', 'materiatransfer');
        }

        if($tableName == "mod_application_form_approver") {
            $tableName = "mod_application_form";
            $this_query = DB::table($tableName);
            $this_query->where('status', '!=', 'FINISHED');
            $this_query->where('status', '!=', 'FAIL');
            $this_query->where('status', '!=', 'BACK');
            $this_query->whereRaw("(manage_by = '". $user->id."' or second_manage in('".$user->id."') ) ");
        }
        if($tableName == "mod_application_form_bymy") {
            
            $tableName = "mod_application_form";
            $this_query = DB::table($tableName);
            $this_query->whereRaw("(created_by = '". $user->id."' or updated_by = '".$user->id."' ) ");
        }
        //20220509各table特殊條件
        if($tableName == "mod_application_list") {
            $this_query->where('created_by', $user->id);
        }

        //20220509各table特殊條件
        if($tableName == "mod_application_form") {
            $this_query->where('hidden_flag','N');
        }

        //20220509各table特殊條件
        if($tableName == "mod_putaway") {
            $this_query->where('status', '!=', 'Y');
        }

        $data[] = array(
            'TotalRows'   =>$this_query->count(),
            'StatusCount' => $status_count
        );

        return $data;
    }
}

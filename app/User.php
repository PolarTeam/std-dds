<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'wh_id', 'company_id', 'theme_color', 'role', 'manage_by', 'manage_name',
        'employee_id', 'lnag','dep','account_enable','g_key','c_key','s_key','d_key'
    ];

    protected $guard_name = 'api';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //驗證帳號欄位
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    public function recInbounds() {
        return $this->hasMany('App\RecInbound', 'created_by');
    }

    public function customers() {
        return $this->hasMany('App\Customer', 'created_by');
    }

    
}

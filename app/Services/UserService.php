<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\UserMgmtRepository;
use App\Repositories\PermissionUsersRepository;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class UserService {

    protected $userRepo;
    protected $permissionUserRepo;
    protected $baseModel;

    public function __construct (
        UserMgmtRepository $userRepo,
        BaseModel $baseModel, PermissionUsersRepository $permissionUserRepo) {
        $this->userRepo           = $userRepo;
        $this->permissionUserRepo = $permissionUserRepo;
        $this->baseModel          = $baseModel;
    }

    public function get ($id) {
        
        $users = $this->userRepo->get($id);
        if ($users) {
            return $users;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {

        $data = $request->all();
        $password =  bcrypt($request->password) ;
        $data['password'] = $password;

        if (isset($data['email'])) {
            $user = $this->userRepo->getByName($data['email']);

            if ($user) {
                throw new Exception(trans('common.msgnew49'));
            }
        }
        $data['role'] = implode(',', $data['role']);
        $users = $this->userRepo->create($data);

        $roles = $this->userRepo->getRoles();
        $user = $this->userRepo->get($users['id']);
        foreach($roles as $role) {
            $user->removeRole($role['name']);
        }

        $roleData = explode(',', $data['role']);
        foreach($roleData as $roleId) {
            $user->assignRole($roleId);;
        }

        if (!$users) {
            throw new Exception('新增失敗');
        } 
        return $users;
    }

    public function update (Request $request, $id) {
        $data         = $request->all();
        $user         = Auth::user();

        if($request->password != null ) {
            $password   = bcrypt($request->password);
            $data['password'] = $password;
        } else {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $data['role'] = implode(',', $data['role']);
        $data['updated_by'] = $user->id;
        $userMgmt        = $this->userRepo->update($id, $data);

        $roles = $this->userRepo->getRoles();
        $user = $this->userRepo->get($id);
        foreach($roles as $role) {
            $user->removeRole($role['name']);
        }

        $roleData = explode(',', $data['role']);
        foreach($roleData as $roleId) {
            $user->assignRole($roleId);;
        }

        if (!$userMgmt) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $users = $this->userRepo->delete($id);

        if (!$users) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $usersId = $request->ids;

        foreach($usersId as $userId) {
            $this->userRepo->delete($userId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('users', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function queryuser() {

        $result  =  $this->userRepo->getByuser();

        return $result;
    }
    public function getUserPermission($userId) {
        // $user = Auth::user();
        $result  =  $this->permissionUserRepo->getByuser($userId);
        return $result;
    }

    public function setDashboardReadList (Request $request) {
        $data         = $request->all();
        $user         = Auth::user();
        $id           = $user->id;
        $data['updated_by'] = $id;

        $userMgmt        = $this->userRepo->update($id, $data);
        if (!$userMgmt) {
            throw new Exception('更新失敗');
        }
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('users', 1, 2000, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'public/users/'.$fileName, 'local');

        return URL::to(Storage::url('users/'.$fileName));
    }

}
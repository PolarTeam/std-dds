<?php

namespace App\Services;

use Exception;
use App\BaseModel;
use App\Exports\BaseExport;
use App\Repositories\PermissionRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class PermissionService {

    protected $permissionRepo;
    protected $baseModel;
    
    public function __construct (PermissionRepository $permissionRepo, BaseModel $baseModel) {
        $this->permissionRepo = $permissionRepo;
        $this->baseModel  = $baseModel;
    }

    public function get ($id) {
        $permission = $this->permissionRepo->get($id);
        if ($permission) {
            return $permission;
        } else {
            throw new Exception(trans('common.search'));
        }
    }

    public function getfiled ($table) {
        
        $filed = $this->baseModel->baseGetFiled($table);
        if ($filed) {
            return $filed;
        } else {
            return null;
        }
    }

    public function create (Request $request) {
        $data = $request->all();
        if (isset($data['name'])) {
            $permission = $this->permissionRepo->getByName($data['name']);

            if ($permission) {
                throw new Exception(trans('common.msgnew36') );
            }
        }
        $permission = $this->permissionRepo->create($data);
        if (!$permission) {
            throw new Exception('新增失敗');
        } 
        return $permission;
    }

    public function update (Request $request, $id) {
        $data = $request->all();
        $user = Auth::user();
        $data['updated_by'] = $user->id;
        $permission = $this->permissionRepo->update($id, $data);
        if (!$permission) {
            throw new Exception('更新失敗');
        }
    }

    public function delete ($id) {
        $permission = $this->permissionRepo->delete($id);

        if (!$permission) {
            throw new Exception('刪除失敗');
        }
    }

    public function batchDelete($request) {
        $permissionIds = $request->ids;

        foreach($permissionIds as $permissionId) {
            $this->permissionRepo->delete($permissionId);
        }

        return true;
    }

    public function query(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $sort          = $request->sort;

        // $baseCondition[] = ['company_id', '=', $user->company_id];
        // $baseCondition[] = ['wh_id', '=', $user->wh_id];

        $result = $this->baseModel->baseQuery('permissions', $request->pageNum, $request->pageSize, $baseCondition, $request->orCondition, $sort, $subSelect);


        return $result;
    }

    public function export(Request $request) {
        $user = Auth::user();

        $baseCondition = $request->baseCondition;
        $subSelect     = array();
        $excelHeaders  = array();
        $dbCols        = array();
        $sort          = $request->sort;
        
        $result        = $this->baseModel->baseQuery('permissions', 1, 2000, $baseCondition, $request->orCondition, $sort, $subSelect);

        $fileName     = $request->fileName.'_'.time().'.xlsx';

        foreach($request->header as $key=> $header) {
            array_push($excelHeaders, $header['filed_text']);
            array_push($dbCols, $header['filed_name']);
        }

        $excelHeaders = isset($excelHeaders) ? $excelHeaders : array('');
        $dbCols       = isset($dbCols) ? $dbCols : array('');
        
        Excel::store(new BaseExport(
            $result['data'], 
            $excelHeaders,
            $dbCols
        ), 'public/permission_export/'.$fileName, 'local');

        return URL::to(Storage::url('permission_export/'.$fileName));
    }

}
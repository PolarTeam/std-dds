<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysAreaModel extends Model
{
    protected $table = 'sys_area';
    protected $fillable = [
        'id',
        'cntry_cd',
        'cntry_nm',
        'state_cd',
        'state_nm',
        'city_cd',
        'city_nm',
        'dist_cd',
        'dist_nm',
        'zip_f',
        'zip_e',
        'remark',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_at',
        'updated_at',
        'updated_by',
        'created_by',
        'station_cd',
        'station_nm',
    ];
}
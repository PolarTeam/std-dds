<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerProfileModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'sys_customers';
    protected $fillable = [
        'id',
        'cust_no',
        'ename',
        'cname',
        'status',
        'cmp_abbr',
        'contact',
        'phone',
        'address',
        'email',
        'remark',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'cust_type',
        'type',
        'zip_code',
        'en_address',
        'website',
        'industry',
        'fax',
        'identity',
        'city_no',
        'city_nm',
        'zip',
        'area_id',
        'area_nm',
        'def',
        'tax_id',
        'receive_mail',
        'email_type',
        'ftppath',
        'ftpport',
        'ftpuser',
        'ftppassword',
        'ftptype',
        'ftpforder',
        'ftpcat',
        'sendmail',
        'phone2',
    ];

}

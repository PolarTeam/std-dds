<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BscodeKind extends Model
{
    protected $table = 'bscode_kind';
    protected $fillable = [
        'id',
        'cd_type',
        'cd_descp',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'adminonly',
    ];

}
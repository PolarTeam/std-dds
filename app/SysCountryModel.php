<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysCountryModel extends Model
{
    protected $table = 'sys_country';
    protected $fillable = [
        'id',
        'cntry_cd',
        'cntry_nm',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_at',
        'updated_at',
        'updated_by',
        'created_by',
    ];
}
<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\MailFormat;
use Carbon\Carbon;
use App\Mail\applyloanMail;
class MailHelper 
{
    public static function sendMail($type , $sendto, $appno = null, $fortype = null, $fromuser = null, $dep = '', $cc = []) {
          // 1	applyloan           申請借料
          // 2	applytransfer       申請轉料
          // 7	applyextension      申請展延
          // 8	applyscrap          申請報廢
          // 9	applyepayment       申請還料
          // 10  auditsuccess	     審核成功
          // 11  auditfail	     審核失敗
          // 12  teanferuser	     點交成功
          // 13  prepar	          備料完成
          // 15  inventory	          盤點
          $keyid ='0';
          $sql = "SELECT * FROM mod_application_form WHERE application_no= '".$appno."' ";
          $formdata = DB::select($sql);
          $applytype = substr($appno,0,2);
          $formatetype = '';

          if($applytype == "CO" ) {
               if($type == "applyloan" ) {
                    $keyid ='17';
                    $formatetype = 'approver';
               } else if($type == "auditsuccess") {
                    if($fortype == "user") {
                         $keyid ='18';
                         $formatetype = 'myapplicationForm';
                    } else {
                         $keyid ='19';
                         $formatetype = 'keeper';
                    }

               } else if($type == "auditfail") {
                    $keyid ='20';
                    $formatetype = 'myapplicationForm';
               } else if($type == "prepar") {
                    $keyid ='21';
                    $formatetype = 'myapplicationForm';
               } else if($type == "teanferuser") {
                  
                    if($fortype == "user") {
                         $keyid ='22';
                         $formatetype = 'myapplicationForm';
                    } else if($fortype == "approver"){
                         $keyid ='23';
                         $formatetype = 'approver';
                    } else if($fortype == "keeper"){
                         $keyid ='24';
                         $formatetype = 'keeper';
                    }
               }
          } else if($applytype == "TF") {
               if($type == "applytransfer" ) {
                    $keyid ='27';
                    $formatetype = 'approver';
               } else if($type == "auditsuccess") {
                    if($fortype == "user1") {
                         $keyid ='28';
                         $formatetype = 'myapplicationForm';
                    } else if($fortype == "user2") {
                         $keyid ='29';
                         $formatetype = 'myapplicationForm';
                    }
               } else if($type == "auditfail") {
                    $keyid ='30';
                    $formatetype = 'myapplicationForm';
               } else if($type == "teanferuser") {
                    if($fortype == "user1") {
                         $keyid ='31';
                         $formatetype = 'myapplicationForm';
                    } else if($fortype == "user2") {
                         $keyid ='33';
                         $formatetype = 'myapplicationForm';
                    } else {
                         $keyid ='32';
                         $formatetype = 'approver';   
                    }
               }
          } else if($applytype == "PP") {
               if($type == "applyextension" ) {
                    $keyid ='34';
                    $formatetype = 'approver';
               } else if($type == "auditsuccess") {
                    $keyid ='35';
                    $formatetype = 'myapplicationForm';
               } else if($type == "auditfail") {
                    $keyid ='36';
                    $formatetype = 'myapplicationForm';
               }
          } else if($applytype == "RT") {
               if($type == "applyepayment" ) {
                    $keyid ='45';
                    $formatetype = 'keeper';
               } else if($type == "teanferuser") {
                    if($fortype == "user") {
                         $keyid ='46';
                         $formatetype = 'myapplicationForm';
                    } else {
                         $keyid ='47';
                         $formatetype = 'keeper';
                    }
               } 
          } else if($applytype == "SC") {
               if($type == "applyscrap" ) {
                    $keyid ='37';
                    $formatetype = 'approver';
               } else if($type == "auditsuccess") {
                    if($fortype == "user") {
                         $keyid ='38';
                         $formatetype = 'myapplicationForm';
                    } else {
                         $keyid ='39';
                         $formatetype = 'keeper';
                    }
               } else if($type == "auditfail") {
                    $keyid ='40';
                    $formatetype = 'myapplicationForm';
               } else if($type == "teanferuser") {
                    if($fortype == "user"){
                         $keyid ='41';
                         $formatetype = 'approver';
                    } else if($fortype == "keeper"){
                         $keyid ='42';
                         $formatetype = 'keeper';
                    } else if($fortype == "approver"){
                         $keyid ='43';
                         $formatetype = 'keeper';
                    }
               }
          } 
          if($type == "inventory" ) {
               $keyid = '49';
               $formatetype = 'inventory';
          }
          if($type == "inventoryfinish" ) {
               $keyid = '50';
               $formatetype = 'inventory';
          }
          if($type == "urge" ) {
               $keyid = '48';
               $formatetype = 'approver';
          }

          $query = DB::table('users');
          $query->where('email', $sendto);
          $sendtodata = $query->first();
          $mailFormat =  MailFormat::find($keyid);
          Log::info($applytype);
          try {
               if($applytype == "CO" ) {
                    if($keyid == '17' ) {
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);

                    } else if($keyid == '18' ) {

                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                    } else if($keyid == '19' ) {
                         $forminfo = DB::table('mod_application_form')->where('application_no', $appno)->first();
                         $user1 = DB::table('users')->where('id', $forminfo->created_by)->first();

                         $mailFormat['title'] = str_replace('{requstername}', $user1->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requstername}', $user1->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);

                    } else if($keyid == '22' ) {
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);
                    } else if($keyid == '23' ||$keyid == '24') {
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);
                    } else {
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                    }

               } else {
                    if($keyid == '32') {
                         // 物料已從{requestername}轉出並掛帳到{user2Name}的物料清單
                         $user1 = DB::table('users')->where('id', $formdata[0]->created_by)->first();
                         $user2 = DB::table('users')->where('id', $formdata[0]->updated_by)->first();
                         $mailFormat['title'] = str_replace('{requestername}', $user1->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $user1->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $user2->name, $mailFormat['content']);
                    } else if($keyid == '40') {
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $fromuser, $mailFormat['content']);

                    } else if ( $keyid == '28' ) {
                         $user1 = DB::table('users')->where('id', $formdata[0]->created_by)->first();
                         $user2 = DB::table('users')->where('id', $formdata[0]->updated_by)->first();

                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);

 
                         $mailFormat['content'] = str_replace('{user2Name}', $user2->name, $mailFormat['content']);

                    }  else if ( $keyid == '29') { 
    
                         $user1 = DB::table('users')->where('id', $formdata[0]->created_by)->first();
                         $user2 = DB::table('users')->where('id', $formdata[0]->updated_by)->first();

                         $mailFormat['title'] = str_replace('{requstername}', $user1->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requstername}', $user1->name, $mailFormat['content']);

                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);

                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $user1->name, $mailFormat['content']);

                    }  else if ( $keyid == '30') { 

                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);

                    } else if ( $keyid == '31' ) {

                         $user2 = DB::table('users')->where('id', $formdata[0]->updated_by)->first();

                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);

                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);

                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $user2->name, $mailFormat['content']);

                    } else if ( $keyid == '33') {
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);

                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);

                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $fromuser, $mailFormat['content']);
                    } else if ( $keyid == '35') {

                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);

                    } else if ( $keyid == '36') {
                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                    } else if ( $keyid == '38' ) {

                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);

                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);


                    }  else if ( $keyid == '39' ) {
                         $user1 = DB::table('users')->where('id', $formdata[0]->created_by)->first();
                         
                         $mailFormat['title'] = str_replace('{requstername}', $user1->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requstername}', $user1->name, $mailFormat['content']);

                         $mailFormat['title'] = str_replace('{approvername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $fromuser, $mailFormat['content']);
                    }  else if ( $keyid == '41' ) {
                         $mailFormat['title'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $sendtodata->name, $mailFormat['content']);
                    }  else if ( $keyid == '42' ) {
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);
                    }  else if ( $keyid == '43' ) {
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);

                    } else {
                         $mailFormat['title'] = str_replace('{requestername}', $fromuser, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{requestername}', $fromuser, $mailFormat['content']);
                         $mailFormat['title'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{approvername}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{username}', $sendtodata->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{user2Name}', $fromuser, $mailFormat['content']);
                    }

               }
               // Log::info($mailFormat['content']);
               //跳轉位置
               // approver
               // myapplicationForm
               // keeper
               // inventory
               if($formatetype == 'approver') {
                    $mailFormat['content'] = str_replace('{applyno}', '<a href="'.url(config('backpack.base.route_prefix', '')).'/en/applicationFormbyapprover/'.$formdata[0]->id.'/edit'.'">'.$appno.'</a>', $mailFormat['content']);
               } else if($formatetype == 'myapplicationForm') {
                    $mailFormat['content'] = str_replace('{applyno}', '<a href="'.url(config('backpack.base.route_prefix', '')).'/en/applicationForm/'.$formdata[0]->id.'/edit'.'">'.$appno.'</a>', $mailFormat['content']);
               } else if($formatetype == 'keeper') {
                    $mailFormat['content'] = str_replace('{applyno}', '<a href="'.url(config('backpack.base.route_prefix', '')).'/en/pickingview/'.$formdata[0]->id.'">'.$appno.'</a>', $mailFormat['content']);
               } else if($formatetype == 'inventory') {
                    $inventory = DB::table('mod_inventory')->where('inventory_no', $appno)
                    ->first();
                    $mailFormat['content'] = str_replace('{applyno}', '<a href="'.url(config('backpack.base.route_prefix', '')).'/en/inventory/'.$inventory->id.'/edit'.'">'.$appno.'</a>', $mailFormat['content']);
                    $mailFormat['title'] = str_replace('{assigned_user_name}', $fromuser, $mailFormat['title']);
                    $mailFormat['content'] = str_replace('{assigned_user_name}', $fromuser, $mailFormat['content']);
                    $mailFormat['content'] = str_replace('{time}', $inventory->inventory_limit_date, $mailFormat['content']);
               }

               // Log::info($mailFormat['content']);
               $mailFormat['title'] = str_replace('{applyno}', $appno, $mailFormat['title']);
               // Log::info($mailFormat['content']);
               $mailFormat['content'] = str_replace('{requstername}', $fromuser, $mailFormat['content']);
               // Log::info($mailFormat['content']);
               $mailFormat['content'] = str_replace('{applydep}', $dep, $mailFormat['content']);
               //code...
               DB::table('mod_mail_schedule')->insert([
                    'application_no' => $appno,
                    'key_id'         => $keyid,
                    'title'          => json_encode($mailFormat['title']),
                    'content'        => json_encode($mailFormat['content']),
                    'sendto'         => $sendto,
                    'cc'             => json_encode($cc),
                    'is_send'        => 'N',
                    'sendcount'      => 0,
                    'created_at'     => Carbon::now()->toDateTimeString(),
               ]);
          } catch (\Throwable $e) {
               Log::info($e->getLine());
               Log::info($e->getMessage());
               // dd($e->getMessage());
               // dd($keyid,$type , $sendto, $appno , $fortype , $fromuser);
               //throw $th;
          }
          


          // if(isset($mailFormat)) {
          //      Mail::to($sendto)
          //      ->cc($cc)
          //      ->send(new applyloanMail($mailFormat,$appno,$formdata));
          // }
          return true;
    }

    public static function sendMailschedule() {
          $sql = "SELECT * FROM mod_mail_schedule WHERE is_send = 'N'  and sendcount <3";
          $notsendmails = DB::select($sql);

          foreach ($notsendmails as $key => $notsendmail) {
               
               try {
                    $mailFormat =  array();
                    $mailFormat['title'] = json_decode($notsendmail->title); 
                    $mailFormat['content'] = json_decode($notsendmail->content); 
                    Mail::to($notsendmail->sendto)
                    ->cc(json_decode($notsendmail->cc))
                    ->send(new applyloanMail($mailFormat,$notsendmail->application_no, null));

                    DB::table('mod_mail_schedule')->where('id', $notsendmail->id)
                    ->update([
                         'is_send'        => 'Y',
                         'sendcount'      => (int)$notsendmail->sendcount+1,
                         'updated_at'     => Carbon::now()->toDateTimeString(),
                    ]);
               } catch (\Throwable $e) {
                    Log::info($e->getLine());
                    Log::info($e->getMessage());
                    DB::table('mod_mail_schedule')->where('id', $notsendmail->id)
                    ->update([
                         'sendcount'      => (int)$notsendmail->sendcount+1,
                         'updated_at'     => Carbon::now()->toDateTimeString(),
                    ]);
               }
          }
    }

    public static function userexpire() {
          try {

               $now =  date("Y-m-d");
               $overdays =  date("Y-m-d",strtotime("-4 day"));
               $expireusers = [];
               $sql = "SELECT * FROM rec_inbound_detail WHERE return_date <= '{$now}' ";
               $normalexpires = DB::select($sql);

               foreach ($normalexpires as $key => $normal) {
                    $expireusers[] = $normal->owner_by;
               }

               $sql = "SELECT * FROM inbound_detail_owner WHERE return_date <= '{$now}' ";
               $conexpires = DB::select($sql);

               foreach ($conexpires as $key => $con) {
                    $expireusers[] = $con->created_by;
               }
               Log::info('log1');
               $expireusers = array_unique($expireusers);
               foreach ($expireusers as $key => $expireuser) {
                    $sendto = DB::table('users')->where('id', $expireuser)
                    ->first();
                    if(isset($sendto)) {
                         Log::info('log1.0');
                         $sql = "SELECT * FROM rec_inbound_detail WHERE return_date <= '{$now}' and owner_by = '{$sendto->id}' ";
                         $usernormalexpires = DB::select($sql);
     
     
                         $sql = "SELECT * FROM inbound_detail_owner WHERE return_date <= '{$now}' and created_by = '{$sendto->id}' ";
                         $userconexpires = DB::select($sql);
     
     
                         $usernormacount = count($usernormalexpires);
                         $userconexpirescount = count($userconexpires);
                         $totalcount =  $usernormacount + $userconexpirescount;
     
     
                         $sql = "SELECT * FROM rec_inbound_detail WHERE return_date <= '{$overdays}' and owner_by = '{$sendto->id}' ";
                         $overnormalexpires = DB::select($sql);
                         Log::info( $sql );
                         $sql = "SELECT * FROM inbound_detail_owner WHERE return_date <= '{$overdays}' and created_by = '{$sendto->id}' ";
                         $overconexpires = DB::select($sql);
                         Log::info( $sql );
                         $isover = "N";
                         $overcount = count($overconexpires) + count($overnormalexpires);
                         if((count($overconexpires) + count($overnormalexpires) ) >0 ) {
                              $isover = "Y";
                         }
                         Log::info('log1.5');
                         Log::info('overcount');
                         Log::info($overcount);
                         $mailFormat =  MailFormat::find('51');
                         Log::info('log1.51');
                         $mailFormat['title'] = str_replace('{username}', $sendto->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{username}', $sendto->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{N}', $totalcount, $mailFormat['content']);
                         Log::info('log1.52');
                         DB::table('mod_mail_schedule')->insert([
                              'key_id'         => '51',
                              'title'          => json_encode($mailFormat['title']),
                              'content'        => json_encode($mailFormat['content']),
                              'sendto'         => $sendto->email,
                              'cc'             => '[]',
                              'is_send'        => 'N',
                              'sendcount'      => 0,
                              'created_at'     => Carbon::now()->toDateTimeString(),
                         ]);
                         Log::info('log1.53');
                         Log::info('log1.6');
                         $mailFormat =  MailFormat::find('52');
                         $mailFormat['title'] = str_replace('{username}', $sendto->name, $mailFormat['title']);
                         $mailFormat['content'] = str_replace('{username}', $sendto->name, $mailFormat['content']);
                         $mailFormat['content'] = str_replace('{N}', $totalcount, $mailFormat['content']);
     
     
                         $query = DB::table('users');
                         $query->where('role', 'like', '%keeper%');
                         $keepers = $query->pluck('email')->toArray();
     
                         foreach ($keepers as $key => $keeper) {
                              DB::table('mod_mail_schedule')->insert([
                                   'key_id'         => '52',
                                   'title'          => json_encode($mailFormat['title']),
                                   'content'        => json_encode($mailFormat['content']),
                                   'sendto'         => $keeper,
                                   'is_send'        => 'N',
                                   'cc'             => '[]',
                                   'sendcount'      => 0,
                                   'created_at'     => Carbon::now()->toDateTimeString(),
                              ]);
                         }
                         Log::info('log1.8');
                         Log::info('log1.8');
                         if($isover == "Y") {
                              Log::info('log1.85');
                              $manager = DB::table('users')->where('email', $sendto->manage_name)
                              ->first();
                              $mailFormat =  MailFormat::find('55');
                              $mailFormat['title'] = str_replace('{username}', $sendto->name, $mailFormat['title']);
                              $mailFormat['content'] = str_replace('{username}', $sendto->name, $mailFormat['content']);
                              $mailFormat['content'] = str_replace('{N}', $overcount, $mailFormat['content']);
     
                              DB::table('mod_mail_schedule')->insert([
                                   'key_id'         => '55',
                                   'title'          => json_encode($mailFormat['title']),
                                   'content'        => json_encode($mailFormat['content']),
                                   'sendto'         => $manager->email,
                                   'is_send'        => 'N',
                                   'cc'             => '[]',
                                   'sendcount'      => 0,
                                   'created_at'     => Carbon::now()->toDateTimeString(),
                              ]);
                         }
                         Log::info('log1.9');
                    }

               }
               Log::info('log2');
          } catch (\Throwable $e) {
               Log::info('userexpire fail by error');
               Log::info($e->getLine());
               Log::info($e->getMessage());
          }
          return;

    }


}
<?php
namespace App\Helpers;

class DesEdeCbc {

    private $cipher, $key, $iv;

    /**
     * DesEdeCbc constructor.
     * @param $cipher
     * @param $key
     * @param $iv
     */
    public function __construct($cipher, $key, $iv) {
        $this->cipher = $cipher;
        $this->key    = $key;
        $this->iv     = $iv;
    }

    /**
     * @func  加密
     * @param $msg
     * @return string
     */
    public function encrypt($msg) {
        $des = @openssl_encrypt($msg, $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->iv);
        return base64_encode($des);
    }

    /**
     * @func  解密
     * @param $msg
     * @return string
     */
    public function decrypt($msg) {
        return @openssl_decrypt(base64_decode($msg), $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->iv);

    }


    /**
     * @func  生成24位长度的key
     * @param $skey
     * @return bool|string
     */
    private function getFormatKey($skey) {
        $md5Value= md5($skey);
        $md5ValueLen = strlen($md5Value);
        $key = $md5Value . substr($md5Value, 0, $md5ValueLen / 2);

        return hex2bin($key);
    }

}
<?php
namespace App\Imports;

use App\Repositories\WarehouseRepository;
use Exception;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class WarehouseImport implements OnEachRow, WithHeadingRow
{
    protected $importTime;
    protected $warehouseRepo;

    public function __construct() {
        $this->warehouseRepo    = new WarehouseRepository;
    }

    public function onRow(Row $row) {
        $user = Auth::user();

        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
        $mappingDetails = $this->getMappingDetail('WAREHOUSE_IMPORT');
        $insertData     = array();
        $insertflag     = 'Y';
        $storageName = '';
        $code='';
        $floorHouseName='';
        $shelfNo='';
        $layer='';
        foreach($mappingDetails as $mappingDetail) {
            if($mappingDetail->col_name == 'code') {
                if(!isset($row[$mappingDetail->excel_col_name])) {
                    throw new Exception('第'.$rowIndex.'行'.$mappingDetail->excel_col_name.'必填');  
                }
                $code = $row[$mappingDetail->excel_col_name];
            }
            if($mappingDetail->col_name == 'floor_house_name') {
                if(!isset($row[$mappingDetail->excel_col_name])) {
                    throw new Exception('第'.$rowIndex.'行'.$mappingDetail->excel_col_name.'必填');  
                }
                $floorHouseName = $row[$mappingDetail->excel_col_name];
            }
            if($mappingDetail->col_name == 'shelf_no') {
                if(!isset($row[$mappingDetail->excel_col_name])) {
                    throw new Exception('第'.$rowIndex.'行'.$mappingDetail->excel_col_name.'必填');  
                }
                $shelfNo = $row[$mappingDetail->excel_col_name];
            }
            if($mappingDetail->col_name == 'layer') {
                if(!isset($row[$mappingDetail->excel_col_name])) {
                    throw new Exception('第'.$rowIndex.'行'.$mappingDetail->excel_col_name.'必填');  
                }
                $layer = $row[$mappingDetail->excel_col_name];
            }

            $storageName = $code.$floorHouseName .'_'. $shelfNo .'_'. $layer;
            $warehouse = $this->chkWarehouseExist($storageName);
            if ( isset($warehouse) ) {
                $insertflag = 'N';
            }

            if(isset($row[$mappingDetail->excel_col_name]) && !empty($row[$mappingDetail->excel_col_name]) && $insertflag == "Y" ) {
                $insertData[$mappingDetail->col_name] = $row[$mappingDetail->excel_col_name];
            }

        }

        $insertData['storage_name']    = $storageName;  //
        $insertData['created_by']      = $user->id;  //
        $insertData['created_by_name'] = $user->name;  //
        $insertData['updated_by']      = $user->id;  //
        $insertData['updated_by_name'] = $user->name;  //
        if( $insertflag == "Y") {
            $this->warehouseRepo->create($insertData);
        } else {
            throw new Exception('第'.$rowIndex.'行'.'，資料儲位已存在'); 
        }
    }

    public function headingRow(): int {
        return 1; //指定第n列為表頭
    }

    public function chkWarehouseExist($storageName) {
        return DB::table('mod_warehouse')
                    ->where('storage_name', $storageName)
                    ->first();
    }

    public function getMappingDetail($mappingCode) {
        return DB::table('mapping_detail')
                ->where('mapping_code', $mappingCode)
                ->orderBy('seq', 'asc')
                ->get();

    }
}

<?php
namespace App\Imports;

use App\Repositories\WindChilRepository;
use Exception;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class WindChilImport implements OnEachRow, WithHeadingRow
{
    protected $importTime;
    protected $windChilRepo;

    public function __construct() {
        $this->windChilRepo    = new WindChilRepository;
    }

    public function onRow(Row $row) {
        $user = Auth::user();

        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
        $mappingDetails = $this->getMappingDetail('WINDCHIL_IMPORT');
        $insertData     = array();
        $updateIds     = array();
        $updateData     = array();
        $insertflag     = 'Y';
 
        foreach($mappingDetails as $mappingDetail) {
           
            if(isset($row[$mappingDetail->excel_col_name]) && $mappingDetail->col_name == 'part_no') {
                // dd($row[$mappingDetail->excel_col_name]);
                $windChil = $this->chkWindChilExist($row[$mappingDetail->excel_col_name]);
                if ( isset($windChil) ) {
                    $insertflag = 'N';
                }
            }

            if(isset($row[$mappingDetail->excel_col_name]) && !empty($row[$mappingDetail->excel_col_name]) && $insertflag == "Y" ) {
                $insertData[$mappingDetail->col_name] = $row[$mappingDetail->excel_col_name];
            } else {
                $updateData[$mappingDetail->col_name] = $row[$mappingDetail->excel_col_name];
                $updateData['updated_by']      = $user->id;
                $updateData['updated_by_name'] = $user->name;
                $updateIds[$windChil->id] = $windChil->id ;
                
            }

        }

        $insertData['created_by']      = $user->id;  //
        $insertData['created_by_name'] = $user->name;  //
        $insertData['updated_by']      = $user->id;  //
        $insertData['updated_by_name'] = $user->name;  //
        if( $insertflag == "Y") {
            $this->windChilRepo->create($insertData);
        }
        foreach ($updateIds as $key => $id) {
            $this->windChilRepo->update($id, $updateData);
        }
    }

    public function headingRow(): int {
        return 1; //指定第n列為表頭
    }

    public function chkWindChilExist($partNo) {
        return DB::table('mod_wind_chil')
                    ->where('part_no', $partNo)
                    ->first();
    }

    public function getMappingDetail($mappingCode) {
        return DB::table('mapping_detail')
                ->where('mapping_code', $mappingCode)
                ->orderBy('seq', 'asc')
                ->get();

    }
}

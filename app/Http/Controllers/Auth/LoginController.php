<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/user';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginView() {
        return view('login', array(
            'loginStatus' => ''
        ));
    }

    public function logout(Request $request) {
        Auth::logout();
    
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if (auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'] , 'account_enable' => 'Y'))) {
            $user = Auth::user();

            session(['locale' => $user->lnag]);
            app()->setLocale($user->lnag);

            $ueserrole = explode(',', $user->role);
            $redirectto = DB::table('bscode')->where('cd_type', 'LOGINPAGE')->whereIn('cd', $ueserrole)->value('value1');
            if(isset($redirectto)){
                return redirect('/'.$redirectto);
            }else{
                return redirect()->route('login')
                ->with('error', 'Email-Address And Password Are Wrong.');
            }
        } else {
            return redirect()->route('login')
                ->with('error', 'Email-Address And Password Are Wrong.');
        }
    }
}

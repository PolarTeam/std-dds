<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    
    public function dashboard() {
       

            $user = Auth::user();
            $title = trans('dashboard.titleName');
            
           
            $userId = $user->id;
 
            $lang = app()->getLocale();

            $viewData = array(
                'myapplicationForm'             => null,
                'myapplicationFormIds'          => null,
                'mymaterials'                   => null,
                'mymaterialsIds'                => null,
                'expiringmaterial'              => null,
                'expiringmaterialIds'           => null,
                'pendingdelivery'               => null,
                'pendingdeliveryIds'            => null,
                'inventoryprofile'              => null,
                'inventoryprofileIds'           => null,
                'keeperpendingapplication'      => null,
                'keeperpendingapplicationIds'   => null,
                'approverpendingapplication'    => null,
                'approverpendingapplicationIds' => null,
                'iskeeper'                      => "N",
                'isapprover'                    => "N",
                'dashboardReadList'             => NULL,
                'userId'                        => $userId,
                'lang'                          => $lang
            );

            return view('dashboard')->with('viewData', $viewData);


    }
    
}

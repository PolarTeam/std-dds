<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Helpers\LogActivity;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\URL;
class FileController extends Controller
{
    public function tranferuploadfile (Request $request, $lang, $applono) {
        
        
    }
    public function uploadfile (Request $request, $lang, $applono) {
        try {
            Storage::makeDirectory('/public/appfile/'.$applono);
            $image = $request->file('uploadfile');
            $ext = $image->extension();
            $image_name = $image->getClientOriginalName();
            Storage::putFileAs('/public/appfile/'.$applono, $image, $image_name);
            
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => "uploadfile sample ",
            'data'    => null,
        ]);
    }

    public function deletefile (Request $request, $lang, $applono) {
        try {
            $files = Storage::allFiles('/public/appfile/'.$applono);
            DB::table('mod_application_detail')
            ->where('file_id', $applono)
            ->update([
                'file_id'    => null,
            ]);
            if( !(is_array($files) && count($files) >0) ) {
               return response()->json([
                   'success' => false,
                   'message' => trans('common.msgnew62'),
                   'data' => null
               ]);
            }
            Storage::deleteDirectory('/public/appfile/'.$applono);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => "get sample success",
            'data'    => null,
        ]);
    }

    public function downloadfile(Request $request, $lang, $applono) {
        try {

             $files = Storage::allFiles('/public/appfile/'.$applono);
             if( !(is_array($files) && count($files) >0) ) {
                return response()->json([
                    'success' => false,
                    'message' => trans('common.msgnew63'),
                    'data' => null
                ]);
             }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
        return response()->json([
            'success' => true,
            'donwloadLink' => URL::to( str_replace('public/','storage/', $files[0])),
            'message' => 'success'
        ]);
    }

}

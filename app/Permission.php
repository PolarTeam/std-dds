<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = [
        'id',
        'name',
        'display_name',
        'type',
        'sorted',
        'guard_name',
        'created_at',
        'updated_at'
    ];
}

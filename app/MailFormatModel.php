<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailFormatModel extends Model
{
    protected $table = 'mail_format';
    protected $fillable = [
        'id',
        'title',
        'public_date',
        'description',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'g_key',
        'c_key',
        's_key',
        'd_key',
    ];
}
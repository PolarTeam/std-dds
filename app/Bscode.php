<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bscode extends Model
{
    protected $table = 'bscode';
    protected $fillable = [
        'cd',
        'cd_descp',
        'cd_type',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'value1',
        'value2',
        'value3',
        'id',
        'remark',
        'token',
    ];

}
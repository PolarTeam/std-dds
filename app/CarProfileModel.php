<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarProfileModel extends Model
{
    protected $table = 'mail_format';
    protected $fillable = [
        'id',
        'cust_no',
        'car_no',
        'car_type',
        'car_type_nm',
        'car_ton',
        'owner',
        'cbm',
        'cbmu',
        'load_rate',
        'load_weight',
        'buy_date',
        'depr_month',
        'depr_desval',
        'depr_bal',
        'driver_no',
        'driver_nm',
        'person_no',
        'person_nm',
        'created_at',
        'updated_at',
        'updated_by',
        'created_by',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'dlv_type',
        'phone',
        'belong_cd',
        'belong_nm',
        'now_gw',
        'now_cbm',
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionUsers extends Model
{
    protected $table = 'permission_users';
    protected $fillable = [
        'user_id',
        'permission_id',
        'permission_name',
    ];
}

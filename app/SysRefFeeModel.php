<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefFeeModel extends Model
{
    protected $table = 'sys_ref_fee';
    protected $fillable = [
        'id',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'chg_cd',
        'chg_descp',
        'gw',
        'cbm',
        'remark',
        'fee_from',
        'fee_to',
        'type',
        'fee_op',
        'fee_weight',
        'trans_type_cd',
        'trans_type_nm',
        'car_type_cd',
        'car_type_nm',
        'distance',
        'regular_fee',
        'fee',
        'fee_unit',
        'second_fee',
        'updated_by',
        'created_by',
        'created_at',
        'updated_at',
        'cust_cd',
        'cust_nm',
        'temperate',
        'temperatenm',
        'start_date',
        'end_date',
        'overcbm',
        'overamt',
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderMgmtModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'mod_order_detail';
    protected $fillable = [
        'id',
        'ord_no',
        'goods_no',
        'goods_nm',
        'pkg_num',
        'pkg_unit',
        'gw',
        'gwu',
        'cbm',
        'cbmu',
        'length',
        'weight',
        'height',
        'updated_by',
        'created_by',
        'created_at',
        'updated_at',
        'goods_no2',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'ord_id',
        'sn_no',
        'price',
        'customerpo',
        'excel_name',
        'swipe_count',
        'box_no',
        'sn_count',
        'goods_type',
    ];

}

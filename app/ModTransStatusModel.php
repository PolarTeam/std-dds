<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModTransStatusModel extends Model
{
    protected $table = 'mod_trans_status';
    protected $fillable = [
        'id',
        'ts_type',
        'order',
        'ts_name',
        'ts_desc',
        'trigger_code',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'created_at',
        'updated_at',
        'updated_by',
        'created_by',
    ];
}
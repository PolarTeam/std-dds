<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DlvModel extends Model
{
    protected $table = 'mod_dlv';
    protected $fillable = [
        'id',
        'dlv_no',
        'ord_no',
        'status',
        'load_rate',
        'load_tweight',
        'load_tcbm',
        'load_tcbmu',
        'dlv_num',
        'dlv_sort',
        'delay_time',
        'cust_no',
        'car_no',
        'car_type_nm',
        'driver_nm',
        'driver_phone',
        'dlv_lat',
        'dlv_lng',
        'created_at',
        'updated_at',
        'updated_by',
        'created_by',
        'g_key',
        'c_key',
        's_key',
        'd_key',
        'car_type',
        'dlv_date',
        'dlv_type',
        'departure_time',
        'finished_time',
    ];
}
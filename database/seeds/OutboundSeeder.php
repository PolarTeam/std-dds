<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\RecOutbound;
use App\OutboundDetail;

class OutboundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $outbound = RecOutbound::create([
            'company_id'        => 8,
            'wh_id'             => 9,
            'outbound_status'   => 10,
            'owner_id'          => 1,
            'order_no'          => Str::random(10),
            'consignee_name'    => Str::random(3),
            'consignee_address' => '台北市大安區新生南路一段163號4樓'
        ]);

        $productId = array(949, 932, 289);

        for($i=0; $i<3; $i++) {
            OutboundDetail::create([
                'outbound_id'     => $outbound->outbound_id,
                'outbound_nums'   => rand(1, 300),
                'product_quality' => 1,
                'product_id'      => $productId[$i]
            ]);
        }
    }
}
